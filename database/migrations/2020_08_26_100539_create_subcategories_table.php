<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategories', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sub_cat_name')->nullable();
            $table->string('sub_cat_arabic_name')->nullable();
            $table->string('parent_cat_id')->nullable();
            $table->string('sequence')->nullable();
            $table->string('slug')->nullable();
            $table->string('meta_data')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategories');
    }
}
