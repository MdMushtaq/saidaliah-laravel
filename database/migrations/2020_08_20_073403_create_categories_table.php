<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cat_name')->nullable();
            $table->string('cat_arabic_name')->nullable();
            $table->string('cat_img')->nullable();
            $table->string('cat_arabic_img')->nullable();
            $table->string('cat_parent')->nullable();
            $table->string('cat_status')->nullable();
            $table->string('cat_seo_title')->nullable();
            $table->string('cat_meta_data')->nullable();
            $table->string('cat_slug')->nullable();
            $table->string('cat_sequence')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
