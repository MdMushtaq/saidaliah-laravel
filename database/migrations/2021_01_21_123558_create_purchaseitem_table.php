<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseitemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchaseitem', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->string('from');
            $table->string('item');
            $table->string('purchase_amount');
            $table->string('model_num');
            $table->string('qty');
            $table->string('price');
            $table->timestamps();
        });
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchaseitem');
    }
}
