<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('short_name')->nullable();
            $table->string('short_ar_name')->nullable();

            $table->string('prod_name')->nullable();
            $table->string('arabic_name')->nullable();
            $table->text('description')->nullable();
            $table->text('arabic_description')->nullable();
            $table->string('model')->nullable();
            $table->string('search_for_related_product_1')->nullable();
            // $table->string('search_for_related_product_2')->nullable();
            $table->string('image')->nullable();
            $table->string('arabic_image')->nullable();
            $table->string('videolink')->nullable();
            $table->string('img_height')->nullable();
            $table->string('track_stock')->nullable();
            $table->string('available_quantity')->nullable();

            $table->text('specification')->nullable();
            $table->text('arabic_specification')->nullable();

            $table->text('additionalinformation')->nullable();
            $table->text('arabic_additionalinformation')->nullable();


            $table->string('newarrival')->nullable();
            $table->string('hotproducts')->nullable();
            $table->string('featuredproducts')->nullable();

            $table->string('url_keyword')->nullable();
            $table->string('title_tag')->nullable();
            $table->string('meta_tag')->nullable();

            $table->string('shipping')->nullable();
            $table->string('brands')->nullable();

            $table->string('sku')->nullable();
            $table->string('weight')->nullable();
            $table->string('retail')->nullable();

            $table->string('price')->nullable();
            $table->string('sale_price')->nullable();
            $table->string('categories_id')->nullable();
            $table->string('sub_category_id')->nullable();
            $table->string('all_offers')->nullable();
            $table->string('promotion_type')->nullable();
            $table->string('warrantyduration')->nullable();
            $table->string('warrantytype')->default(0);
            $table->string('status')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
