<?php

return [
    // checkout-form.blade.php

    'Checkout' => 'الدفع',
    'Need Some Informtion' => 'بحاجة الى بعض المعلومات',
    'Account' => 'الحساب',
    'Address' => 'عنوان',
    'Payment' => 'دفع',
    'Cart - item' => 'عربة التسوق - البند',
    'Total' => 'مجموع',
    'Subtotal:' => 'المجموع الفرعي:',
    'SR' => 'ريال',
    'VAT' => 'ضريبة القيمة المضافة',
    'Total:' => 'مجموع:',
    'Payment & Shipping Address' => 'عنوان الدفع والشحن',
    'Name' => 'اسم',
    'Optional' => 'اختياري',
    'Mobile Number' => 'رقم الهاتف المحمول',
    'Country' => 'بلد',
    'City' => 'مدينة',
    'Shipping to another address' => 'الشحن إلى عنوان آخر',
    'Save Address' => 'حفظ العنوان',
    'I have read and agree to the Terms & Conditions' => 'لقد قرأت ووافقت على الشروط والأحكام',
    'There are no items in this cart' => 'لا توجد بنود في هذه العربة',
    'Continue Shoping' => 'مواصلة التسوق',
    'Email' => 'البريد الإلكتروني',
    'Shipping Price' => 'سعر الشحن',
];

?>