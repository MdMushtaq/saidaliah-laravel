<?php

return [
    // checkout-form.blade.php

    'Checkout' => 'Checkout',
    'Need Some Informtion' => 'Need Some Informtion',
    'Account' => 'Account',
    'Address' => 'Address',
    'Payment' => 'Payment',
    'Cart - item' => 'Cart - item',
    'Total' => 'Total',
    'Subtotal:' => 'Subtotal:',
    'SR' => 'SR',
    'VAT' => 'VAT',
    'Total:' => 'Total:',
    'Payment & Shipping Address' => 'Payment & Shipping Address',
    'Name' => 'Name',
    'Optional' => 'Optional',
    'Mobile Number' => 'Mobile Number',
    'Country' => 'Country',
    'City' => 'City',
    'Shipping to another address' => 'Shipping to another address',
    'Save Address' => 'Save Address',
    'I have read and agree to the Terms & Conditions' => 'I have read and agree to the Terms & Conditions',
    'There are no items in this cart' => 'There are no items in this cart',
    'Continue Shoping' => 'Continue Shoping',
    'Email' => 'Email',
    'Shipping Price' => 'Shipping Price',


];

?>