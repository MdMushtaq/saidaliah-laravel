@extends('layouts.admin-app')
@section('content')

@if(count($errors))
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <br/>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">
<div class="page-header">
    <h1>Customers</h1>
</div>


<div class="btn-group pull-right">
    <br>
    <a class="btn btn-primary" href="{{ url('/admin/customers/form') }}"><i class="icon-plus"></i> Add New Customer</a>
    <a class="btn btn-primary" style="margin-left: 5px;" href="{{ url('/admin/customers/archive') }}"> Archive Customer</a>
    <a class="btn btn-primary" style="margin-left: 5px;" href="{{ url('/admin/customers/suspend') }}"> Suspend Customer</a>
    <br>
</div>
    <br>
<form action="{{ url('admin/customers/exporttoexcelcustomers') }}" method="POST">
    @csrf
    <input  class="btn btn-primary" type="submit" value="Export to Excel"/>
        <table id="testTable" class="table table-striped">
        <thead>
        <tr>
            <th><input type="checkbox" onclick="toggle(this)" /> Check All</th>
            <th>S. No.</th>
            <th>Joining Date</th>
            <th>Name</th>
            <th>Mobile Number</th>
            <th>Email </th>
            {{-- <th>Gender</th> --}}
            <th>Country</th>
            <th>City</th>
        </tr>
        </thead>

        <tbody>

        <?php
        $count = 1;
        foreach($customers as $customer)
        {
            ?>
            <tr>
                <td>
                    <input type="checkbox" name="customerids[]" value="{{ $customer->id }}" class="customeridcheckbox">
                </td>
                <td><?= $count; ?></td>
                <td><?= $customer->created_at->format('Y-m-d'); ?></td>
            
                <td class="gc_cell_left"><?= $customer->name; ?></td>
                
                <td class="gc_cell_left"><?= $customer->phone; ?></td>
                <td class="gc_cell_left"><?= $customer->email; ?></td>
                <td class="gc_cell_left"><?= $customer->country; ?></td>
                <td class="gc_cell_left"><?= $customer->city; ?></td>
                <td class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-default" title="Edit Customer" href="{{ url('admin/customers/form/') }}<?= '/'.$customer->id; ?>"><i class="icon-pencil"></i></a>
                        {{-- <a class="btn btn-default" title="View Customer Address" href="/admin/customers/addresses/72974"><i class="icon-envelope"></i></a> --}}
                    </div>
                </td>
            </tr>
        <?php
            $count++;
        }
        ?>


        </tbody>
        </table>
</form>

</div>
<script>
    function toggle(source)
    {
        checkboxes = document.getElementsByClassName('customeridcheckbox');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
</script>
@endsection
