@extends('layouts.admin-app')
@section('content')
@if(count($errors))
<div class="alert alert-danger">
   <strong>Whoops!</strong> There were some problems with your input.
   <br/>
   <ul>
	   @foreach($errors->all() as $error)
		   <li>{{ $error }}</li>
	   @endforeach
   </ul>
</div>
@endif 


<div class="page-header">
	<h1>Create Purchase Item</h1>
</div>
<form action="{{route('purchase.store')}}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
	@csrf
	<div class="row">
    	<div class="col-md-12">
        	<div class="row">
            	<div class="col-md-4">
               		<div class="form-group">
                		<label for="date">Date</label>
                		<input type="date" name="date" required class="form-control">
              		</div>
            	</div>
	            <div class="col-md-4">
	            	<div class="form-group">
	               		<label for="from">From</label>
	               		<input type="text" name="from" required class="form-control">
	            	</div>
                </div>
                <div class="col-md-4">
	            	<div class="form-group">
	               		<label for="item">item</label>
	               		<input type="text" name="item" required class="form-control">
	            	</div>
	            </div>
        	</div>
        </div>
        
        <div class="col-md-12">
        	<div class="row">
            	<div class="col-md-3">
               		<div class="form-group">
                		<label for="p_amount">Purchase Amout</label>
                		<input type="text" name="p_amount" required class="form-control">
              		</div>
            	</div>
	            <div class="col-md-3">
	            	<div class="form-group">
	               		<label for="model_no">Model No</label>
	               		<input type="text" name="model_no" required class="form-control">
	            	</div>
                </div>
                <div class="col-md-3">
	            	<div class="form-group">
	               		<label for="qty">Qty</label>
	               		<input type="text" name="qty" required class="form-control">
	            	</div>
                </div>
                <div class="col-md-3">
	            	<div class="form-group">
	               		<label for="price">Price</label>
	               		<input type="text" name="price" required class="form-control">
	            	</div>
	            </div>
        	</div>
			
		</div>
    </div>
    
	<div class="row">
	    <div class="col-md-10">
	        <button type="submit" name="submit" class="btn btn-primary">Save</button>
	    </div>
	    <div class="col-md-2"></div>
	</div>
</form>


@endsection