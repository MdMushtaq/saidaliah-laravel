@extends('layouts.admin-app')
@section('content')
@if (\Session::has('success'))
<div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
   <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif 
<div class="page-header">
  <h1>Purchase Products</h1>
</div>

<div class="text-right form-group">
    <a class="btn btn-primary" style="font-weight:normal;" href="{{route('purchase.create')}}">
    	<i class="icon-plus"></i>  Add Purchase Product
    </a>
</div>
<table  id="myTable" class="table table-striped">
    <thead>
        <tr>
            <th>Date</th>
            <th>From</th>
            <th>Item</th>
            <th>Purchase Amount</th>
            <th>Model No</th>
            <th>Qty</th>
            <th>Price</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
       @foreach ($itempurchase as $item)
            <tr>
                <td>{{$item->date}}</td>
                <td>{{$item->from}}</td>
                <td>{{$item->item}}</td>
                <td>{{$item->purchase_amount}}</td>
                <td>{{$item->model_num}}</td>
                <td>{{$item->qty}}</td>
                <td>{{$item->price}}</td>
                <td class="text-right">
                    <div class="btn-group">

                        <a class="btn btn-default" href="{{ route('purchase.edit',$item->id) }}" alt="Edit">
                            <i class="icon-pencil"></i>
                        </a>


                        <a class="btn btn-danger" href="{{ route('purchase.delete',$item->id) }}" onClick="confirm('Are your sure You Want to delete this product?')" alt="Delete">
                            <i class="icon-times"></i>
                        </a>

                    </div>
                </td>
            </tr>
       @endforeach
    </tbody>
       
</table>

@endsection
