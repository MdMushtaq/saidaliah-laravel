@extends('layouts.admin-app')
@section('content')

<h3>Order Items</h3>
<table class="table">
    <tbody class="orderItems">
        @foreach ($itemorder as $order)
            @foreach ($order->orderitems as $item)
                <tr>
                    <td>
                    <strong>{{$item->product_name}}</strong> <br>

                    </td>
                <td></td>
                    <td>
                        <div style="font-size:11px; color:#bbb;">({{$item->product_quantity}}  ×  {{$item->product_unit_price}})</div>SR {{ number_format($item->product_price, 2)}}
                    </td>
                </tr>
            @endforeach

        @endforeach
       
    </tbody>
   
    <tbody class="orderTotals">
        <td colspan="2">
            <div style="font-size:17px;">Total</div>
        </td>
            <td colspan="2">
                <div style="font-size:17px;">SR {{ number_format($order->total, 2) }}</div>
            </td>
        </tr>
      	<tr>
            <td colspan="2" class="cartSummaryTotalsKey tex">VAT :</td>
            <td colspan="2" class="cartSummaryTotalsValue text-right">SR {{number_format($order->vat, 2)}}</td>
        </tr>
        <tr>
            <td colspan="2">Shipping Price</td>
            <td>SR {{ number_format($order->shipping_price, 2)}}</td>
        </tr>
        <tr>
            <td colspan="2">Subtotal</td>
            <td>SR {{ number_format($order->subtotal, 2)}}</td>
        </tr>
    </tbody>
</table>

@php $userinfo = App\User::where('id',$order->user_id)->first(); @endphp

<div class="page-header">
	<h1>Order </h1>
</div>


<div style="margin:10px 0px;">
    <div class="row">
        <div class="col-md-3">
            <h3>Shipping Address</h3>
           

            @if($order->shipping_name == '' && $order->shipping_phone == '' && $order->shipping_address == '' )
            <strong> {{$userinfo->name }}</strong><br>
            <small>
                {{$userinfo->phone}}
                <br>
                {{$userinfo->email }}
                <br>
                {{$userinfo->country }} 
                <br>
                {{$userinfo->city }} 
            </small>
            <br>
            <small> {{$userinfo->address }} </small>
           
            @else
           
            <small>
                {{$order->shipping_name}}
                <br>
                {{$order->shipping_phone}}
                <br>
                {{$order->shipping_address}}
            </small>   
            @endif
        </div>
        <div class="col-md-3">
            <h3>Billing Address</h3>
            <strong> {{$userinfo->name .' '. $userinfo->lastname}}</strong><br>
            <small>
                {{$userinfo->phone}}
                <br>
                {{$userinfo->email }}
                <br>
                {{$userinfo->country }} 
                <br>
                {{$userinfo->city }} 
            </small>
            <br>
            <small> {{$userinfo->address }} </small>
        </div>
        <div class="col-md-3">
            <h3>Payment</h3>
            <div>{{$order->payment}}</div>
        </div>
        <div class="col-md-3">
            <form action="{{route('order.update',$order->id)}}" method="post" accept-charset="utf-8">
                @csrf
                <input type="hidden" value="{{$userinfo->email}}" name="user_email">
                
                <div class="form-group">
                    <label>Status</label>
                    <select name="order_approved_status" class="form-control" id="order_approved_status">
                        <option value="">Select</option>

                        <option value="Confirm_byAdmin" {{$order->approved_status == 'Confirm_byAdmin' ? 'selected' : ''}}>Confirm By Admin</option>
                        <option value="Confirm_byAjent" {{$order->approved_status == 'Confirm_byAjent' ? 'selected' : ''}}>Confirm By Ajents</option>
                    
                    </select>
          		</div>
 				<div class="form-group">
    				<label>Order Status</label>
                        <select name="order_status" class="form-control" id="order_status">
                        <option value="Processing" {{$order->status == 'Processing' ? 'selected' : ''}}>Processing</option>
                        <option value="Shipped" {{$order->status == 'Shipped' ? 'selected' : ''}}>Shipped</option>
                        <option value="Cancled" {{$order->status == 'Cancled' ? 'selected' : ''}}>Cancled</option>
                        <option value="Pending" {{$order->status == 'Pending' ? 'selected' : ''}}>Pending</option>
                        <option value="Delivered" {{$order->status == 'Delivered' ? 'selected' : ''}}>Delivered</option>
                        <option value="Dispatched" {{$order->status == 'Dispatched' ? 'selected' : ''}}>Dispatched</option>

                    </select>
                </div>
                <div class="form-group">
                    <label for="aramex_ref">Aramex Refrence</label>
                    <input type="text" class="form-control" value="{{$order->aramex_ref}}" name="aramex_ref" id="aramex_ref">
                </div>
                <div class="form-group">
                    <label for="aramex_tracking">Aramex Tracking</label>
                    <input type="text" class="form-control" value="{{$order->aramex_track}}" name="aramex_tracking" id="aramex_tracking">
                </div>
                <div class="form-group">
                    <label for="shipping_date">Shipping date</label>
                    <input type="date" class="form-control" value="{{$order->shipping_date}}" name="shipping_date" id="shipping_date">
                </div>
                <input type="submit" class="btn btn-primary" value="Update Order"/>
            </form>
        </div>
    </div>
</div>

<br><br><br><br>

@endsection