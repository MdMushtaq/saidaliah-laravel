@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>{{$headingTitle}}</h1>
</div>

    <div class="well">
        <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="input-order-id">Order Date</label>
                        <input type="date" name="filter_order_id" value="" placeholder="Order ID"  id="myInput" class="form-control" onchange="myFunction()">
                    </div>

                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="input-customer">Customer</label>
                        <input type="text" name="filter_customer" value="" placeholder="Customer" id="input-customer" class="form-control" autocomplete="off"  onkeyup="myFunction1()"><ul class="dropdown-menu"></ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="input-order-status">Order Status</label>
                        <select name="filter_order_status" id="input-order-status" class="form-control" onchange="myFunction3()">
                            <option value=""></option>

                                <option value="Processing">Processing</option>
                                <option value="Shipped">Shipped</option>
                                <option value="Canceled">Canceled</option>
                                <option value="Pending">Pending</option>
                                <option value="Delivered">Delivered</option>
                                <option value="Dispatched">Dispatched</option>

                            <option value="#">Confirmed By Admin</option>
<!--                            <option value="#">Confirmed by Account</option>-->
                        </select>
                    </div>

                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="input-total">Total</label>
                        <input type="text" name="filter_total" value="" placeholder="Total" id="input-total" class="form-control" onkeyup="myFunction2()">
                    </div>
                </div>

                <a class="btn btn-primary pull-right space" href="{{url('/admin/orders')}}">Reset</a>
               
        </div>
    </div>
<form action="{{ route('orders.exporttoexcel') }}" method="POST">
    @csrf
    <button class="btn btn-primary" type="submit">Export to Excel</button>

    <table class="table table-striped" id="myTable">
        <thead>
            <tr>
                <th><input type="checkbox" onclick="toggle(this)" /> Check All</th>
                <th><a href="#">Order Date</a></th>
                <th><a href="#">Customer Name</a></th>
                <th><a href="#">Order Status</a> </th>
                <th><a href="#">Payment</a> </th>
                <th><a href="#">Status</a></th>
                <th><a href="#">Total</a></th>
                <th>VAt</th>
                <th>Grant total</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
                <td>
                    <input type="checkbox" name="orderids[]" value="{{ $order->id }}" class="ordersidcheckbox"> </td>
                <td>{{ $order->created_at->format('Y-m-d') }}</td>
                <td>{{$order->user->name }}</td>
                <td style="max-width:200px;">{{$order->status}}
                    {{-- <select name="order_status" disabled class="form-control" id="order_status">
                        <option value="Processing" {{$order->status == 'Processing' ? 'selected' : ''}}>Processing</option>
                        <option value="Shipped" {{$order->status == 'Shipped' ? 'selected' : ''}}>Shipped</option>
                        <option value="Cancled" {{$order->status == 'Cancled' ? 'selected' : ''}}>Cancled</option>
                        <option value="Pending" {{$order->status == 'Pending' ? 'selected' : ''}}>Pending</option>
                        <option value="Delevered" {{$order->status == 'Delevered' ? 'selected' : ''}}>Delevered</option>
                        <option value="Dispatched" {{$order->status == 'Dispatched' ? 'selected' : ''}}>Dispatched</option>

                    </select> --}}
                </td>
                <td style="max-width:200px;">{{$order->payment}}</td>
                <td style="max-width:200px;">
                    <select name="order_approved_status" disabled class="form-control" id="order_approved_status">
                        <option value="" {{$order->approved_status == '' ? 'selected' : ''}}>Not Approved</option>

                        <option value="Confirm_byAdmin" {{$order->approved_status == 'Confirm_byAdmin' ? 'selected' : ''}}>Confirm By Admin</option>
                        <option value="Confirm_byAjent" {{$order->approved_status == 'Confirm_byAjent' ? 'selected' : ''}}>Confirm By Ajents</option>
                    
                    </select>
                </td>
                <td>
                    <div class="MainTableNotes total7222">SR {{ $order->vat }}</div>
                </td>
                <td>
                    <div class="MainTableNotes total7222">SR <?= $order->total; ?></div>
                </td>
                <td>
                    <div id="total7222">SR <?= $order->subtotal; ?></div>
                </td>
                <td class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-default" href="{{route('orderitem.show',$order->id)}}">
                            <i class="icon-eye"></i>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
</form>
<script>

    function toggle(source)
    {
        checkboxes = document.getElementsByClassName('ordersidcheckbox');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }

    function myFunction() {

        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function myFunction1() {

        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("input-customer");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function myFunction2()
    {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("input-total");
        filter = input.value.toUpperCase();
        filter = "SR "+filter;
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[5];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function myFunction3()
    {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("input-order-status");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>
@endsection