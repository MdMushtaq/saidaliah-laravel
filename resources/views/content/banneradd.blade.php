@extends('layouts.admin-app')
@section('content')

<div class="page-header">
    <h1>Add New Banner Collections</h1>
</div>
@if(count($errors))
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <br/>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@elseif($message = Session::get('bannerslimit'))
<div class="alert alert-danger">
    <strong>Error!</strong> {{ $message }}
</div>
@endif
<form action="{{ url('admin/banners/addpost') }}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="image">Image </label>
                <div class="input-append">
                    <input type="file" name="banner_image" class="form-control">
                </div>

            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="image">Arabic Image </label>
                <div class="input-append">
                    <input type="file" name="banner_arabic_image" class="form-control">
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="link">Slider Link</label>
                <div class="input-append">
                    <input type="text" name="slider_link" required class="form-control">
                </div>

            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-2">
            <input class="btn btn-primary" type="submit" value="Save">
        </div>
        <div class="col-md-10"></div>
    </div>
</form>
@endsection