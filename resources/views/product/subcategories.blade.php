@extends('layouts.admin-app')
@section('content')

@if (\Session::has('success'))
<div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
   <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif 

    <div class="container">
      
        <div class="page-header"><h1>Sub Category</h1></div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-md-4"></div>
                   

                    <div class="col-md-8">
                        <form action="#" class="form-inline form-group" style="float:right" method="post" accept-charset="utf-8">
                       
                            <div class="form-group" >
                                @php $catgetname = App\Categories::where('cat_status',1)->get(); @endphp
                                <select id="filter-company" class="filter form-control">
                                    <option value="0">Filter By Category</option>
                                    @foreach ($catgetname as $item)
                                        <option value="{{$item->cat_name}}">{{$item->cat_name}}</option>
                                    @endforeach
                                </select>
                               
                                <input type="text"  id="myInput" onkeyup="myFunction()" class="form-control" name="term" value="" placeholder="Search Subcategorie">
                            </div>
<!--                            <button class="btn btn-default" name="submit" value="search">Search</button>-->
                            <a class="btn btn-default" href="#">Reset</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-right form-group">
            <a class="btn btn-primary" style="font-weight:normal;" href="{{ url('admin/subcategories/form') }}">
                <i class="icon-plus"></i> Add New Sub Category
            </a>

            <a class="btn btn-danger" style="font-weight:normal;" href="{{ route('trash.subcategories') }}">
                <i class="icon-times"></i> Trash Sub Category
            </a>

        </div>
        <table class="table table-striped" id="myTable">
            <thead>
            <tr>
                <th><a href="#">Category Name <i class="icon-sort-alt-down"></i></a></th>
                <th><a href="#">Category Arabic Name</a></th>
                <th><a href="#">Subcategory Name</a></th>
                <th><a href="#">Subcategory Arabic Name</a></th>
                <th style="width:16%">

                </th>
            </tr>
            </thead>

            <tbody>

            
            @foreach($subcategories as $subcat)
                @php $catgetname = App\Categories::where('id',$subcat->parent_cat_id)->get(); @endphp
            <tr>
                @foreach ($catgetname as $item)
                <td class="company" data-company="{{$item->cat_name}}">{{$item->cat_name}}</td>
                <td>{{$item->cat_arabic_name}}</td>
                @endforeach
             
                <td>{{ $subcat->sub_cat_name}}</td>
                <td>{{$subcat->sub_cat_arabic_name}}</td>
               
                <td class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-default" href="{{ url('admin/subcategories/edit',$subcat->id) }}" alt="Edit"><i class="icon-pencil"></i></a>
                       
                        @if( $subcat->status == '0')
                        <a class="btn btn-danger" href="{{ route('subcategories.delete',$subcat->id) }}" onclick="return confirm('are you sure?')" alt="delete"><i class="icon-times"></i></a>
                        @endif
                    </div>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

    </div>
<script>
    function myFunction() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>

<script>
 $('.filter').change(function(){

filter_function();

//calling filter function each select box value change

});

$('table tbody tr').show(); //intially all rows will be shown

function filter_function(){
$('table tbody tr').hide(); //hide all rows

var companyFlag = 0;
var companyValue = $('#filter-company').val();
var contactFlag = 0;
var contactValue = $('#filter-contact').val();
 var rangeFlag = 0;
var rangeValue = $('#filter-range').val();
 var rangeminValue = $('#filter-range').find(':selected').attr('data-min');
 var rangemaxValue = $('#filter-range').find(':selected').attr('data-max');

//setting intial values and flags needed

//traversing each row one by one
$('table tr').each(function() {  

  if(companyValue == 0){   //if no value then display row
  companyFlag = 1;
  }
  else if(companyValue == $(this).find('td.company').data('company')){ 
    companyFlag = 1;       //if value is same display row
  }
  else{
    companyFlag = 0;
  }
  
  
 if(companyFlag ){
   $(this).show();  //displaying row which satisfies all conditions
 }

});

}
</script>
@endsection
