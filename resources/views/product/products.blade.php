@extends('layouts.admin-app')
@section('content')
@if (\Session::has('success'))
<div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
   <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif 
<div class="page-header">
    {{-- <h1>{{$trash->status = 0 ? 'Deleted prduct' : 'Products' }}}</h1> --}}
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                &nbsp;
            </div>
            <div class="col-md-8">
                @php $category_filter = App\Categories::all(); @endphp
                <form action="{{route('categories.filter')}}" class="form-inline form-group" style="float:right" method="post" accept-charset="utf-8">
                  @csrf
                    <div class="form-group">
                        <select class="form-control" id="category_filter" required name="category_filter">
                            <option value="" style="display: none;">Filter by Category</option>
                            <option value="newarrival"> --> New Arrival</option>
                            <option value="hotproducts"> --> Hot Products</option>
                            <option value="featuredproducts"> --> Featured Products</option>

                            @foreach ($category_filter as $filters)

                            @php
                            $totalProducts = App\products::where('categories_id', $filters->id)->get()->count();
                            @endphp

                            <option value="{{$filters->id}}"> {{ $filters->cat_name }}({{ $totalProducts }}) </option>
                            @endforeach

                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="subcategory_filter" name="subcategory_filter">
                            <option value="" style="display: none;">Select</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" id="myInput" onkeyup="myFunction()" class="form-control" name="term" placeholder="Search Term">
                    </div>
                    <button class="btn btn-default" name="submit" value="search">Search</button>
                    <a class="btn btn-default" href="{{url('admin/products')}}">Reset</a>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="text-right form-group">
    <a class="btn btn-primary" style="font-weight:normal;" href="{{route('add.products')}}">
    	<i class="icon-plus"></i>   New Product
    </a>
    <a class="btn btn-danger" href="{{ route('trash.products')}}">
        <i class="icon-times"></i> Trash Product
    </a>
</div>
<table  id="myTable" class="table table-striped">
    <thead>
        <tr>
            <th>English Image</th>
            <th>Arabic Image</th>
            
            <th onclick="sortTable(0)">
                <a href="#">Name
                    <i class="icon-sort-alt-down"></i>
                </a>
            </th>
            <th onclick="sortTable(1)">
                <a href="#">Arabic Name</a>
            </th>

            <th onclick="sortTable(2)">
                <a href="#">Image Height</a>
            </th>

            <th onclick="sortTable(3)">
                <a href="#">Total Quantity</a>
            </th>
            <th onclick="sortTable(4)">
                <a href="#">Created Date</a>
            </th>
            <th onclick="sortTable(5)">
                <a href="#">Status</a>
            </th>
            @can('Products.editproductpost')
             <th style="width:16%"></th>
            @endcan
        </tr>
    </thead>
        <tbody>
        @foreach ($products as $product)
            @php $folder = App\Categories::where('id',$product->categories_id)->first(); @endphp
        <tr>
            <td>
                @foreach (json_decode($product->image) as $key => $products)
                @if($key == 0)
                    <img src="{{asset('public/images/products/'.$folder->cat_name.'/'.$products)}}" width="30" height="30" alt="">
                @endif
                @endforeach
                
            </td>

            <td>
                @foreach (json_decode($product->arabic_image) as $key => $arb_products)
                @if($key == 0)
                    <img src="{{asset('public/images/products/'.$folder->cat_name.'/'.$arb_products)}}" width="30" height="30" alt="">
                @endif
                @endforeach 
            </td>
          
            
        <td>{{$product->prod_name}}</td>
            <td>{{$product->arabic_name}}</td>

            <td>{{$product->img_height}} </td>
            <td>

                @foreach ($product->productQuantity->groupBy('productid') as $item)
                    <input type="text" name="" value="{{$item->sum('quantity')}}" readonly class="form-control tableInput">
                @endforeach
               

                
            </td>
            <td> {{$product->created_at->format('y/m/d')}}</td>
            <td>{{$product->status == 1 ? 'Enable' : 'Disable'}}</td>

            
                <td class="text-right">
                    <div class="btn-group">
                    
                        <a class="btn btn-default" href="{{ url('admin/products/edit',$product->id) }}" alt="Edit">
                            <i class="icon-pencil"></i>
                        </a>

                        @if($product->status == 0)

                        <a class="btn btn-danger" href="{{ route('product.delete',$product->id) }}" onClick="confirm('Are your sure You Want to delete this product?')" alt="Delete">
                            <i class="icon-times"></i>
                        </a>
                        @endif
                            
                    </div>
                </td>
            
        </tr>
      @endforeach
        </tbody>
</table>

    <script>
        function myFunction() {
          var input, filter, table, tr, td, i, txtValue;
          input = document.getElementById("myInput");
          filter = input.value.toUpperCase();
          table = document.getElementById("myTable");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
              txtValue = td.textContent || td.innerText;
              if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }       
          }
        }

    </script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function(){
        $("#category_filter").change(function(){
            var $catId = this.value;

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getsubcategories') }}",
                data: {categoryid:$catId},
                success: function(result)
                {
                    $('#subcategory_filter').html(result);
                }
            });
        });
    });
</script>

@endsection
