@extends('layouts.admin-app')
@section('content')


@if (\Session::has('success'))
<div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
</div>
<br />

@elseif(\Session::has('danger'))
<div class="alert alert-danger">
   <p>{{ \Session::get('danger') }}</p>
</div>
<br />
@endif 


<div class="page-header">
	<h1>Sub Category Wise Offers & Promotions</h1>
</div>
<a class="btn btn-primary" style="float:right;" href="{{ url('admin/sub_category_wise_offers/form') }}">
	<i class="icon-plus"></i> Add New Category Offer
</a>
<table class="table">
    <thead>
        <tr>
          <th>Offer Name</th>
          <th>Category Name</th>
          <th>Sub-Category Name</th>
          <th>Enable Date</th>
          <th>Disable Date</th>
          <th>Reduction Type</th>
          <th>Reduction Amount</th>
          <th>Status</th>
          <th></th>
        </tr>
    </thead>
    <tbody>


        <?php
        foreach($category_wise_discounts as $discount)
        {
            ?>
            <tr>
                <td><?= $discount->offer_name; ?></td>
                <td>
                    <?php
                    $maincategory = App\Categories::where('id',$discount->category_id)->first();
                    echo $maincategory->cat_name;
                    ?>
                    </td>
                <td>
                    <?php
                    $subcategory = App\subcategories::where('id',$discount->sub_category_id)->first();
                    echo $subcategory->sub_cat_name;
                    ?>
                </td>
                <td><?= $discount->enable_on_utc; ?></td>
                <td><?= $discount->disable_on_utc; ?></td>
                <td><?= $discount->reduction_type; ?></td>
                <td><?= $discount->percentage; ?></td>
                <td><?php if($discount->status == 1) { echo "Enabled"; } else { echo "Disabled"; } ?></td>
                <td class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-default" href="{{ url('/admin/sub_category_offers/edit/') }}<?= '/'.$discount->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    <a href="{{ route('sub_categories_offer.delete',$discount->id) }}" class="btn btn-danger" onclick="return confirm('are you sure?')">
                        <i class="icon-times"></i>
                    </a>
                    </div>
                </td>
            </tr>
        <?php
        }
        ?>

    </tbody>
</table>
    
@endsection