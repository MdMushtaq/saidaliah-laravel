@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>Shipping Modules</h1>
</div>
<table class="table table-striped">
    <tbody>
        <tr>
        	<td>Vat Rates</td>
            <td>
                <span class="btn-group pull-right">
                    <a class="btn btn-default" href="{{ url('admin/vat/form') }}">
                       	<i class="icon-gear"></i>
                    </a>
                    <a class="btn btn-danger" href="#" onclick="">
                    	<i class="icon-times"></i>
                    </a>
                </span>
            </td>
        </tr>
        <tr>
        	<td>Shipping Rate</td>
            <td>
                <span class="btn-group pull-right">
                    <a class="btn btn-default" href="{{ route('shipping.list') }}">
                       	<i class="icon-gear"></i>
                    </a>
                    <a class="btn btn-danger" href="#" onclick="">
                    	<i class="icon-times"></i>
                    </a>
                </span>
            </td>
        </tr>
         {{-- <tr>
        	<td>Flat Rate</td>
            <td>
                <span class="btn-group pull-right">
                    <a class="btn btn-default" href="{{ url('admin/flat-rate/form') }}">
                       	<i class="icon-gear"></i>
                    </a>
                    <a class="btn btn-danger" href="#" onclick="">
                    	<i class="icon-times"></i>
                    </a>
                </span>
            </td>
        </tr> --}}
	</tbody>
</table>
@endsection