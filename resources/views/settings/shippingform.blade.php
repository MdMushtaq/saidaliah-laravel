@extends('layouts.admin-app')
@section('content')
<div class="page-header">
    <h1>Shipping Configuration</h1>
</div>
@if(\Session::has('danger'))
<div class="alert alert-danger">
    <p>{{ \Session::get('danger') }}</p>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="text-right form-group">
        <a class="btn btn-primary" href="{{route('add.shipping')}}">
                <i class="icon-plus"></i> Add Shipping
            </a>
        </div>
    </div>
</div>
<table class="table table-striped" id="myTable">
    <thead>
        <tr>
            <th>Name</th>
            <th>Arabic Name</th>
            <th>Shipping rate</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($city as $cities)
            @php $shipping = $cities->Shipping; @endphp
        <tr>
            <td>{{$cities->name}}</td>
            <td>{{$cities->arabic_name}}</td>
            <td>SR :{{$shipping->shippingrate}}</td>
            <td>{{$cities->status == 1 ? 'Enable' : 'Disable'}}</td></td>
            <td>
                <a class="btn btn-default" href="{{ route('shipping.edit',$cities->id) }}" alt="Edit">
                    <i class="icon-pencil"></i>
                </a>
            </td>
        </tr>
        @endforeach
        
      
    </tbody>
</table>
@endsection