@extends('layouts.admin-app')
@section('content')
<div class="page-header">
    <h1>Product Sales Report</h1>
</div>
<div class="row">
    <br>
    <div class="col-md-6">
        {{-- <h3>Best Sellers</h3> --}}
        <form style="margin-top: -2%;">
            <div class="form-group">
                <label>Search by item: </label>
                <input style="display: inline-block; width: 43%;" type="text" class="form-control" id="myInput" onkeyup="myFunction()">
            </div>
        </form>
    </div>
    <div class="col-md-6">
        <form action="{{ route('report.serch') }}" class="form-inline pull-right" method="POST">
            {{ csrf_field() }}
            <input class="form-control datepicker" type="text" name="best_sellers_start" value="{{old('best_sellers_start')}}" placeholder="From"/>
            <input class="form-control datepicker" type="text" name="best_sellers_end" placeholder="To"/>
            <input  class="btn btn-primary" type="submit" value="Search"/>
            <a class="btn btn-primary" href="{{url('/admin/sales/productsalesreport')}}">Reset</a>
        </form>
    </div>
</div>
<br>
<form action="{{ route('exporttoexcelproductsalesreport') }}" method="POST">
    @csrf
    <input  class="btn btn-primary" type="submit" value="Export to Excel"/>
<table class="table table-striped" cellspacing="0" cellpadding="0" id="myTable">
    <thead>
    <tr>
        <th><input type="checkbox" onclick="toggle(this)" required />  Check All</th>
        <th>Date</th>
        <th>Full Name</th>
        <th>Item</th>
        <th>Unit Price</th>
        <th>Quantity</th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $total = 0;
    ?>
    @foreach($orders as $order)
    @foreach ($order->orderitems as $item)
    <tr>
        <td>
            <input type="checkbox" name="order_id[]" value="{{ $order->id }}" class="orderidcheckbox" disabled>
        </td>
        <td>{{ $order->created_at->format('d/m/Y') }}</td>

        <td>{{ $order->user->name }}</td>
        <td>
            {{ $item->product_name }}
        </td>
        <td>{{ $item->product_unit_price }}</td>
        <td>{{ $item->product_quantity }}</td>
        <td>{{ $item->product_unit_price*$item->product_quantity }}</td>
        <?php
        $totalAmount = $item->product_unit_price*$item->product_quantity;
        $total = $total+$totalAmount;
        ?>
        <td class="text-right">
            <div class="btn-group">
                <a class="btn btn-default" href="{{route('orderitem.show',$order->id)}}">
                    <i class="icon-eye"></i>
                </a>
            </div>
        </td>
    </tr>
    @endforeach
    @endforeach
    </tbody>
</table>
    </form>

<div class="row">
    <div class="col-md-8"></div>
    <div class="col-md-4">
        <table class="table" border="1">
            <tr>
                <td>Total Amount</td>
                <td><span>{{ $total }}</span></td>
            </tr>
        </table>
    </div>
</div>

<script>
    function myFunction() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[3];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function toggle(source)
    {
        checkboxes = document.getElementsByClassName('orderidcheckbox');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    


</script>


@endsection