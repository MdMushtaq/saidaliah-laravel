@extends('layouts.admin-app')
@section('content')
<div class="page-header">
    <h1>Vat Report</h1>
</div>

<div class="row">
    <br>
    <div class="col-md-6">
    </div>
    <div class="col-md-6">
        <form action="{{ url('admin/sales/vatreport') }}" class="form-inline pull-right" method="POST">
            {{ csrf_field() }}
            <input class="form-control datepicker" type="text" name="best_sellers_start" value="{{old('best_sellers_start')}}" placeholder="From"/>
            <input class="form-control datepicker" type="text" name="best_sellers_end" placeholder="To"/>
            <input  class="btn btn-primary" type="submit" value="Get Best Sellers"/>
        </form>
    </div>
</div>
<br>
<form action="{{ route('exporttoexcelvatreport') }}" method="POST">
    @csrf
    <input  class="btn btn-primary" type="submit" value="Export to Excel" required/>
<table class="table table-striped" id="myTable">
    <thead>
    <tr>
        <th><input type="checkbox" onclick="toggle(this)" required /> Check All</th>
        <th><a href="#">Order Date</a></th>
        <th><a href="#">Customer Name</a></th>
        <th><a href="#">VAt</a></th>
        <th>Total</th>
        <th>Grant total</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>

    @foreach($orders as $order)

    <tr>
        <td><input type="checkbox" name="order_id[]" value="{{ $order->id }}" class="orderidcheckbox" disabled> </td>
        <td>{{ $order->created_at->format('Y-m-d') }}</td>
        <td>{{$order->user->name }}</td>
        <td>
            <div class="MainTableNotes total7222">SR {{ $order->vat }}</div>
        </td>
        <td>
            <div class="MainTableNotes total7222">SR <?= $order->total; ?></div>
        </td>
        <td>
            <div id="total7222">SR <?= $order->subtotal; ?></div>
        </td>
        <td class="text-right">
            <div class="btn-group">
                <a class="btn btn-default" href="{{route('orderitem.show',$order->id)}}">
                    <i class="icon-eye"></i>
                </a>
            </div>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4">
            <table class="table" border="1">
                <tr>
                    <td>Total Vat</td>
                    <td><span>{{ number_format($totalVat,2) }}</span></td>
                </tr>
            </table>
        </div>
    </div>
</form>



<script>
    function myFunction() {

        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function myFunction1() {

        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("input-customer");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function myFunction2()
    {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("input-total");
        filter = input.value.toUpperCase();
        filter = "SR "+filter;
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[5];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function myFunction3()
    {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("input-order-status");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function toggle(source)
    {
        checkboxes = document.getElementsByClassName('orderidcheckbox');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }

</script>
@endsection