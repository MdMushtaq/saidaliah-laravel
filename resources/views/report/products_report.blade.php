@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>Products Info</h1>
</div>
<div class="row">
    <br>
    <div class="col-md-6">
       
        <label>Search by Name</label>
        <input style="display: inline-block; width: 43%;" type="text" class="form-control" id="myInput" onkeyup="myFunction()">
        
    </div>
<!--    <div class="col-md-6">-->
<!--        <form class="form-inline pull-right">-->
<!--            <input class="form-control datepicker" type="text" name="best_sellers_start" placeholder="From"/>-->
<!--            <input class="form-control datepicker" type="text" name="best_sellers_end" placeholder="To"/>-->
<!---->
<!--            <input class="btn btn-primary" type="button" value="Get Best Sellers"/>-->
<!--        </form>-->
<!--    </div>-->
</div>
<br>
<div class="btn-group pull-right">
</div>
<br>
<form action="{{ route('exporttoexcelproductsreport') }}" method="POST">
    @csrf
    <input  class="btn btn-primary" type="submit" value="Export to Excel"/>
    <table class="table table-striped" cellspacing="0" cellpadding="0" id="myTable">
        <thead>
        <tr>
            <th><input type="checkbox" onclick="toggle(this)" required /> Check All</th>
            <th>Product Name</th>
            <th>Product Color(s)</th>
            <th>Color Quantity</th>
            <th>Sale</th>
            <th>Balance</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($best_sell as $report)
            @php
            //  $saleout = App\Orderitem::where('product_id',$report->id)->get();
            //  $order = App\Orders::where('id',$saleout->order_id)->first();

            $names = [];
            $quantities = [];
            $saleQuantities = [];
            $balanceQuantities = [];
            foreach ($report->productQuantity as $attribute) {
                $attribute->items_quantity = $attribute->items_quantity ?? 0;
                $names[] = $attribute->colourname;
                $quantities[] = $attribute->quantity ?? 0;
                $saleQuantities[] = $attribute->items_quantity;
                $balanceQuantities[] = $attribute->quantity - $attribute->items_quantity;

                // if($order->status == 'Delivered')
                // {
                // $saleQuantities[] = $attribute->items_quantity;
                // $balanceQuantities[] = $attribute->quantity - $attribute->items_quantity;
                // }
                // else{
                //     $saleQuantities[] = 0;
                //     $balanceQuantities[] = $attribute->quantity ?? 0;
                // }
            }
            @endphp
                <tr>
                    <td><input type="checkbox" name="productids[]" value="{{ $report->product_id }}" class="productidscheckbox" disabled> </td>
                    <td>{{$report->prod_name}}</td>
                    <td>{{implode(', ', $names)}}</td>
                    <td>{{implode(', ', $quantities)}}</td>
                    <td>{{implode(', ', $saleQuantities)}}</td>
                    <td>{{implode(', ', $balanceQuantities)}}</td>
            @endforeach
        </tbody>
    </table>
 </form>
<script>
    function myFunction() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function toggle(source)
    {
        checkboxes = document.getElementsByClassName('productidscheckbox');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
</script>
@endsection