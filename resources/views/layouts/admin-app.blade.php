    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="{{asset('public/assets/ionicons-2.0.1/css/ionicons.min.css')}}">
        <title>TechnoSouq :: Dashboard</title>
        <link href="{{asset('public/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('public/assets/css/admin.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('public/assets/css/font-awesome.css')}}" rel="stylesheet" type="text/css">
        <link type="text/css" href="{{asset('public/assets/css/redactor.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('public/assets/css/pickadate/default.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('public/assets/css/pickadate/default.date.css')}}" rel="stylesheet">
        <script type="text/javascript" src="{{asset('public/assets/js/jquery-2.1.3.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/custom.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/jquery-ui.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/picker.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/picker.date.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/picker.time.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/redactor.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/imagemanager.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/spin.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/mustache.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/redactor_lang/english.js')}}"></script>
        <link rel="stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}">
        <link type="text/css" href="{{asset('public/assets/css/custom.css')}}" rel="stylesheet">
        <link type="text/css" href="{{asset('public/assets/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
        <script type="text/javascript" src="{{asset('public/assets/js/bootstrap-datetimepicker.js')}}"></script>
        <script src="{{asset('public/assets/js/socket.io.js')}}"></script>
        <script src="{{asset('public/assets/js/sorting.js')}}"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
          
        <script>
        $(document).ready(function(){
        $('.dropdown-submenu a.test').on("click", function(e){
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
            });
        });
        </script>
        <script type="text/javascript">
        $(document).ready(function(){
            $('.datepicker').pickadate({formatSubmit:'yyyy-mm-dd', hiddenName:true, format:'mm/dd/yyyy'});
            //$('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
            $(".form_datetime").datetimepicker({
                format: "yyyy-mm-dd hh:ii",
                autoclose: true,
                todayBtn: true,
                showMeridian: true,
                pickerPosition: "bottom-left"
            });
            
            $('.redactor').redactor({
                lang: 'english',
                minHeight: 200,
                pastePlainText: true,
                linebreaks:true,
                imageUpload: '',
                imageManagerJson: '',
                imageUploadErrorCallback: function(json){
                    alert(json.error);
                },
                plugins: ['imagemanager']
            });
        });
        </script>
        <link rel="stylesheet" type="text/css" href="{{asset('public/assets/css/stylesheet.css')}}">
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                <a class="navbar-brand" href="{{url('/admin')}}"><img src="{{asset('public/logo.png')}}" style="margin-top: -30px;" height="80" width="80" alt=""></a>
                </div>
                <div class="collapse navbar-collapse" id="primary-navbar">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="{{url('/admin')}}">Dashboard</a>
                        </li>
                        <li class="dropdown">
                            @can('Products')
                                <a href="dashboard" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Products 
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    @can('Categories')
                                        <li>
                                            <a href="{{url('/admin/categories')}}">Categories</a>
                                        </li>
                                    @endcan
                                    @can('Sub Category')
                                    <li>
                                        <a href="{{url('/admin/subcategories')}}">Sub Category</a>
                                    </li>
                                    @endcan
                                    @can('Brands')
                                    <li>
                                        <a href="{{url('/admin/brands')}}">Brands</a>
                                    </li>
                                    @endcan
                                    @can('Product')
                                    <li>
                                        <a href="{{url('/admin/products')}}">Products</a>
                                    </li>
                                    @endcan
                                    <li>
                                        <a href="{{route('purchase.show')}}">Purchase Product</a>
                                    </li>
                                    {{-- @can('Information')
                                    <li>
                                        <a href="{{url('/admin/pages')}}">Information</a>
                                    </li>
                                    @endcan --}}
                                </ul>
                            @endcan
                        </li>
                        <li class="dropdown">
                            @can('Promotions')
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Promotions 
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    @can('Product wise % discount')
                                    <li>
                                        <a href="{{ url('admin/product_offers') }}">Product wise % discount</a>
                                    @endcan
                                    {{-- <li>
                                        <a href="{{url('/admin/category_offers')}}">Category wise % discount</a>
                                        </li> --}}
                                   
                                    @can('Sub Category wise % discount')
                                    <li>

                                    <a href="{{url('/admin/sub_category_offers')}}">Sub Category wise % discount</a>
                                    </li>
                                    @endcan
                                    {{-- @can('Brand wise % discount')
                                    <li>
                                    <a href="{{url('/admin/brand_offers')}}">Brand wise % discount</a>
                                    </li>    
                                    @endcan
                                    @can('Coupons')         
                                    <li>
                                        <a href="{{url('/admin/coupons')}}">Coupons</a>
                                    </li>
                                    @endcan --}}
                                </ul>
                            @endcan
                        </li>
                        <li class="dropdown">
                            @can('Sales')
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Sales 
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    @can('Orders')
                                    <li>
                                        <a href="{{url('/admin/orders')}}">Orders</a>
                                    </li>
                                    @endcan
                                    @can('Confirmed By Admin')
                                    <li>
                                        <a href="{{url('/admin/orders/confirmbyAdmin')}}">Confirmed By Admin</a>
                                    </li>
                                    @endcan
                                    @can('Delivered')
                                    <li>
                                        <a href="{{route('order.delivered')}}">Delivered</a>
                                    </li>
                                    @endcan
                                    {{-- @can('Stores')
                                    <li>
                                        <a href="{{url('/admin/stores')}}">Stores</a>
                                    </li> 
                                    @endcan --}}
                                </ul>
                            @endcan
                        </li>
                        <li class="dropdown">
                            @can('Content')
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Content 
                                    <span class="caret"></span>
                                </a>
                                
                                <ul class="dropdown-menu" role="menu">
                                    @can('Slider')
                                    <li>
                                        <a href="{{url('/admin/banners')}}">Slider</a>
                                    </li>
                                    @endcan
                                    @can('Images')
                                    <li>
                                        <a href="">Images</a>
                                    </li>
                                    @endcan
                                </ul>
                            @endcan
                        </li>
                        @can('Wishlist')
                        <li>
                            <a href="{{ url('admin/wishlist') }}">Wishlist</a>
                        </li>
                        @endcan
                        @can('Customer')
                        <li>
                            <a href="{{ url('admin/customers') }}">Customer</a>
                        </li>
                        @endcan
                        @can('Reports')
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Report 
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{url('/admin/products/report')}}">Products Info</a>
                                </li>
                                <li>
                                    <a href="{{url('/admin/sales/report')}}">Sales Info</a>
                                </li>
                                <li>
                                    <a href="{{url('/admin/sales/productsalesreport')}}">Product Sales Info</a>
                                </li>
                                <li>
                                    <a href="{{url('/admin/sales/vatreport')}}">Vat Report</a>
                                </li>
                            </ul>
                        </li>
                        @endcan
                        <li class="dropdown">
                            @can('Setting')
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Setting 
                                    <span class="caret"></span>
                                </a>
                          
                                <ul class="dropdown-menu" role="menu">
                                    {{-- @can('Configuration')
                                    <li>
                                        <a href="{{url('/admin/configuration')}}">Configuration</a>
                                    </li>
                                    @endcan --}}
                                    @can('Shipping Modules')
                                    <li>
                                        <a href="{{url('/admin/shipping')}}">Shipping Modules</a>
                                    </li>
                                    @endcan
                                    @can('Payment Modules')
                                    <li>
                                        <a href="{{url('/admin/payments')}}">Payment Modules</a>
                                    </li>
                                    @endcan
                                    {{-- @can('Cash on Delivery Charges')
                                    <li>
                                        <a href="{{url('/admin/locations')}}">Cash on Delivery Charges</a>
                                    </li>
                                    @endcan --}}
                                    {{-- @can('Canned Messages')
                                    <li>
                                        <a href="{{url('/admin/canned_messages')}}">Canned Messages</a>
                                    </li>
                                    @endcan --}}
                                    @can('Administrators')
                                    <li>
                                        <a href="{{url('/admin/users')}}">Administrators</a>
                                    </li>
                                    @endcan
                                    @can('Sitemap')
                                    <li>
                                        <a href="{{url('/')}}">Sitemap</a>
                                    </li>
                                    @endcan
                                    <li>
                                        <a href="{{url('/admin/faq')}}">Faq</a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/subscriptions')}}">Subscriptions</a>
                                    </li>
                                </ul>
                            @endcan
                        </li>
                        <li class="dropdown">
                        <a href="{{url('/')}}" target="_blank">Front End</a>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> 
                                {{ Auth::guard('admin')->user()->name  }}<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                                     {{ __('Logout') }}
                                 </a>
 
                                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                     @csrf
                                 </form>
                                </li>
                            </ul>
                        </li>
                       
                    </ul>
                </div>
            </div>
        </nav>
        
        <div class="container">
            <div id="js_error_container" class="alert alert-error" style="display:none;">
                <p id="js_error"></p>
            </div>
            <div id="js_note_container" class="alert alert-note" style="display:none;"></div>
        </div>
        
        <div class="container">
            @yield('content')
        </div>
        
    </body>


</html>