<!DOCTYPE html>
<html lang="en">
<head>
    <title>Techno Souq</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  Favicon Icon -->
    <link href="{{asset('public/frontend/img/favicon.png')}}" rel="shortcut icon" type="image/png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    @if(\Session::get('locale') == 'ar')

    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.1.3/css/bootstrap.min.css" integrity="sha384-Jt6Tol1A2P9JBesGeCxNrxkmRFSjWCBW1Af7CSQSKsfMVQCqnUVWhZzG0puJMCK6" crossorigin="anonymous">
   
    @else
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    @endif
    <!--  Bootstrap -->
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    <!--  Flag Css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">

    <!--  Zoom Css -->
   
     <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/css/xzoom.css')}}" media="all" />
    
    
    <!--  Zoom Fancybox -->
    <link type="text/css" rel="stylesheet" media="all" href="{{asset('public/frontend/fancybox/source/jquery.fancybox.css')}}" />

    <!--  Magnific Popup -->
    <link type="text/css" rel="stylesheet" media="all" href="{{asset('public/frontend/magnific-popup/css/magnific-popup.css')}}" />
    
    <!--  Product Css -->
    @if(\Session::get('locale') == 'ar')
    <link href="{{asset('public/frontend/ar/product-description.css')}}" media="screen" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/ar/home.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/ar/rtl.css')}}">
    <link rel="stylesheet" href="{{asset('public/frontend/ar/custom.css')}}" media="screen" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/ar/ar-media-query.css')}}" media="all" />
    @else
    <link href="{{asset('public/frontend/css/product-description.css')}}" media="screen" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/css/home.css')}}" />
    <link rel="stylesheet" href="{{asset('public/frontend/css/custom.css')}}" media="screen" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/css/media-query.css')}}" media="all" />
    @endif 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" type="text/css" media="all" />
    

    {{-- Tap Payment Api Link --}}
    <link href="https://goSellJSLib.b-cdn.net/v1.6.0/css/gosell.css" rel="stylesheet" />

    {{-- token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
    <body>
        {{-- english Header --}}
        <header class="bg-primary container-fluid ">
            <div class="container-fluid overflow-hidden" >
                <!-- Logo -->
                <div class="row justify-content-center align-items-center header">
                    <div class="col-xl-2 col-sm-12 p-0 text-center">
                        <a href="{{url('/')}}">
                            <img src="{{asset('public/frontend/img/logo.png')}}" class="logo" alt="">
                        </a>
                    </div>
                    <div class="d-none d-xl-block col-xl-8 col-sm-12 p-0">
                        <div class="row">
                            <div class="col-xl-8">
                                <form id="search-form" class="form-inline my-lg-0 d-none d-md-block" role="form" action="{{ route('search') }}" method="POST">
                                    @csrf
                                    <div class="input-group">
                                        <input type="text" class="form-control search-form" placeholder="Search" name="search">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn search-btn" data-target="#search-form" name="q">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xl-4">
                                <ul class="nav main-nav langue">
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle p-0" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-top: 2px;">
                                            <img src="{{asseT('public/frontend/img/language.png')}}" /><i class="fa fa-caret-down text-right" ></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="dropdown-item" href="{{url('localization/en')}}">English</a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item" href="{{url('localization/ar')}}">العربية</a>
                                            </li>
                                        </ul>
                                    </li>
                                    @guest
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle p-0" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-top: 2px;">
                                            <i class="fa fa fa-user-circle-o" style="font-size: 2.0em;"></i> <span style="position: relative;top: -5px;">{{ trans('welcome.SIGN IN')}}</span> <i class="fa fa-caret-down text-right" style="position: relative;top:-5px;"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            
                                            @if (Route::has('register'))
                                                <li>
                                                    <a class="dropdown-item" href="{{url('/login')}}">{{ trans('welcome.SIGN IN')}}</a>
                                                </li>
                                                <li>
                                                    <a class="dropdown-item" href="{{url('/register')}}">{{ trans('welcome.SIGN UP')}}</a>
                                                </li>
                                            @endif
                                        </ul>
                                    </li>
                                    @else
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle p-0" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-top: 2px;">
                                            <i class="fa fa fa-user-circle-o" style="font-size: 2.0em;"></i> <span style="position: relative;top: -5px;">{{ substr(Auth::user()->name, 0, 8) }} </span> <i class="fa fa-caret-down text-right" style="position: relative;top:-5px;"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="dropdown-item" href="{{route('customer.dashboard')}}">Dashboard</a>
                                            </li>
                                            <li>
                                                <form action="{{ route('logout') }}" method="POST">
                                                    @csrf
                                                    <button class="dropdown-item" type="submit">
                                                        {{ __('Logout') }}
                                                    </button>
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                    @endguest
                                </ul>
                            </div>
                        </div>    
                    </div>
                    <div class="d-none d-xl-block col-xl-2 col-sm-12">
                        <div class="cart">
                            {{ trans('welcome.CART')}}
                           <a href="{{route('checkout.cart')}}" class="fa-stack fa-2x has-badge" data-count="{{Cart::count()}}">
                            <img src="{{asseT('public/frontend/img/cart-1.png')}}" />
                           </a>
                        </div>    
                    </div>
                </div>
                <!-- End Logo -->
            </div>    
        </header>
        
        {{-- mobile Header --}}
        <div class="mobile-header bg-primary">
            <nav class="navbar navbar-expand-md navbar-light">
                <a href="{{url('/')}}">
                    <img src="{{asset('public/frontend/img/logo.png')}}" class="logo" alt="">
                </a>
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link" href="{{ url('/about') }}"> {{ trans('welcome.ABOUT US')}}</a>
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">{{ trans('welcome.CATEEGORIES')}} <i class="fa fa-caret-down text-right"></i></a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">{{ trans('welcome.Electronics')}}</a>
                            </div>
                        </div>
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">  {{ trans('welcome.PRODUCTS')}}<i class="fa fa-caret-down text-right"></i></a>
                            <div class="dropdown-menu">
                                @php  $catagories = App\Categories::where('cat_status', 1)->get();  @endphp
                                @foreach ($catagories as $item)
                                    <li >
                                        <a class="dropdown-item" href="{{route('category.list',$item->id)}}">
                                            @if(\Session::get('locale') == 'ar')
                                            {{$item->cat_arabic_name}}
                                            @else
                                            {{$item->cat_name}}
                                            @endif
                                        </a>
                                        <ul class="dropdown-menu" >
                                            @php $subcat = App\subcategories::where(['status' => 1 ,'parent_cat_id' => $item->id])->get(); @endphp

                                            @foreach ($subcat as $subitem)
                                            <li>
                                                <a class="dropdown-item " href="{{route('subcategory.list',$subitem->id)}}">
                                                    @if(\Session::get('locale') == 'ar')
                                                    {{$subitem->sub_cat_arabic_name}}
                                                    @else
                                                    {{$subitem->sub_cat_name}}
                                                    @endif
                                                    </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </div>
                        </div>
                        <a class="nav-item nav-link" href="{{ url('/help') }}">{{ trans('welcome.HELP')}}</a>
                        <a class="nav-item nav-link" href="{{ url('/contact') }}">{{ trans('welcome.CONTACT US')}}</a>
                        <a class="nav-item nav-link" href="{{ url('/deals') }}">{{ trans('welcome.DEALS')}}</a>
                    </div>
                   
                    <div class="navbar-nav ml-auto">
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"><img src="{{asseT('public/frontend/img/language.png')}}" /> <i class="fa fa-caret-down text-right"></i></a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{url('localization/en')}}">English</a>
                                <a class="dropdown-item" href="{{url('localization/ar')}}">العربية</a>
                            </div>
                        </div>
                        @guest
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">  <i class="fa fa fa-user-circle-o" style="font-size: 2.0em;"></i> {{ trans('welcome.SIGN IN')}} <i class="fa fa-caret-down text-right"></i></a>
                            <div class="dropdown-menu">
                                @if (Route::has('register'))
                                    <a class="dropdown-item" href="{{url('/login')}}">{{ trans('welcome.SIGN IN')}}</a>
                                    <a class="dropdown-item" href="{{url('/register')}}">{{ trans('welcome.SIGN UP')}}</a>
                                @endif
                            </div>
                        </div>
                        @else
                        <div class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"> <i class="fa fa fa-user-circle-o" style="font-size: 2.0em;"></i> {{ substr(Auth::user()->name, 0, 8) }} <i class="fa fa-caret-down text-right"></i></a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{route('customer.dashboard')}}">Dashboard</a>
                                <form action="{{ route('logout') }}" method="POST">
                                    @csrf
                                    <button class="dropdown-item" type="submit">
                                        {{ __('Logout') }}
                                    </button>
                                </form>
                            </div>
                        </div>
                        @endguest
                        {{-- Delivery --}}
                        <i class="fa fa-map-marker marker " aria-hidden="true"></i>
                        <a href="#" class="marker-link">
                            <strong style="color:#fff;">DELIVER TO</strong>
                            <span style="color:#fff;" class="d-block mb-3"> SAUDIA ARABIA
                            </span>
                        </a> 
                        {{-- /end Delivery --}}

                    </div>
                </div>
            </nav>
            <div class="row">
                <div class="col-xl-12">
                    <form id="search-form" class="form-inline my-lg-0 d-md-block" role="form" method="post" action="">
                        <div class="input-group">
                            <input type="text" class="form-control search-form" placeholder="Search">
                            <span class="input-group-btn">
                                <button type="submit" class="btn search-btn" data-target="#search-form" name="q">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>    
            {{-- cart --}}
            <div class="row mobile-nav">
                <div class="col-sm-6">
                    <div class="cart">
                        {{ trans('welcome.CART')}}
                        <a href="{{route('checkout.cart')}}"  class="fa-stack fa-2x has-badge" data-count="{{Cart::count()}}">
                            <img src="{{asseT('public/frontend/img/cart-1.png')}}">  
                        </a>
                    </div>   
                </div>
                
                <div class="col-sm-6 flash-m">
                    <span>
                        <a class="flash-sale" href="#">
                            {{ trans('welcome.FLASH')}}
                            <i class="fa fa-bolt" style="color:#e37236;margin-left:5px;margin-right:5px;"></i>
                            {{ trans('welcome.SALE')}}
                            <span class="clock">
                                <span id="days1"></span>:
                                <span id="hours1"></span>:
                                <span id="minutes1"></span>:
                                <span id="seconds1"></span>
                            </span>
                        </a>
                    </span>
                </div>
                
            </div>   
            {{-- \end cart --}}
        </div>
        {{-- End mobile Header  --}}

        <div class="container-fluid nav-bg desktop-nav">
            <div class="row">
                <!-- Primary Navigation Bar Desktop -->
                <div class="col-md-2">
                    <i class="fa fa-map-marker marker" aria-hidden="true">
                        </i>
                    <a href="#" class="marker-link">
                        <strong style="color:#fff;">{{ trans('welcome.DELIVER TO')}}</strong><span style="color:#fff;" class="d-block"> {{ trans('welcome.SAUDIA ARABIA')}} </span>
                    </a>    
                </div>
                <div class="col-md-7">
                    <nav class="navbar navbar-expand-lg navbar-hover" id="main_navbar">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="main_nav">
                            <ul class="navbar-nav main-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/about') }}"> {{ trans('welcome.ABOUT US')}}</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ trans('welcome.CATEEGORIES')}} <i class="fa fa-caret-down text-right"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a class="dropdown-item" href="#">{{ trans('welcome.Electronics')}}</a>
                                        </li>

                                    </ul>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ trans('welcome.PRODUCTS')}} <i class="fa fa-caret-down text-right"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        @php  $catagories = App\Categories::where('cat_status', 1)->get();  @endphp
                                        @foreach ($catagories as $item)
                                            <li >
                                                <a class="dropdown-item" href="{{route('category.list',$item->id)}}">
                                                    @if(\Session::get('locale') == 'ar')
                                                    {{$item->cat_arabic_name}}
                                                    @else
                                                    {{$item->cat_name}}
                                                    @endif
                                                </a>
                                                <ul class="dropdown-menu" >
    
                                                    @php $subcat = App\subcategories::where(['status' => 1 ,'parent_cat_id' => $item->id])->get(); @endphp
    
                                                    @foreach ($subcat as $subitem)
                                                    <li>
                                                        <a class="dropdown-item " href="{{route('subcategory.list',$subitem->id)}}">
                                                            @if(\Session::get('locale') == 'ar')
                                                            {{$subitem->sub_cat_arabic_name}}
                                                            @else
                                                            {{$subitem->sub_cat_name}}
                                                            @endif
                                                            </a>
                                                    </li>
                                                    @endforeach
                                                
                                                </ul>
                                            </li>
                                        @endforeach
                                       
                                    </ul>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/deals') }}">{{ trans('welcome.DEALS')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/help') }}">{{ trans('welcome.HELP')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/contact') }}">{{ trans('welcome.CONTACT US')}}</a>
                                    
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>

                <div class="col-md-3 flash-m" >
                    <span>
                        <a class="flash-sale" href="#">
                            {{ trans('welcome.FLASH')}}
                            <i class="fa fa-bolt" style="color:#e37236;margin-left:5px;margin-right:5px;"></i>
                            {{ trans('welcome.SALE')}} &nbsp;
                            &nbsp;
                            <span class="clock">
                                <span id="days"></span>:
                                <span id="hours"></span>:
                                <span id="minutes"></span>:
                                <span id="seconds"></span>
                            </span>
                        </a>
                            {{-- <span class="clock">{{ trans('welcome.END IN')}}  00:55:04</span> --}}
                        </a>
                    </span>
                </div>
               
                <!-- End Primary Navigation Bar Desktop -->
            </div>
        </div>
       
        <div>
            @yield('content')
        </div>

<!-- Footer -->    
<section id="footer">
    <div class="container">
        <div class="row text-center text-xs-center text-sm-left text-md-left" >
            <div class="col-xs-12 col-sm-3 col-md-3 text-center">
                <a href="#">
                    <img src="{{asset('public/frontend/img/footer-logo.png')}}" class="logo" alt="">
                </a>
            </div>

            <div class="col-xs-12 col-sm-3 col-md-3">
                <h5>{{ trans('welcome.Quick links')}}</h5>
                <ul class="list-unstyled quick-links">
                    {{-- <li>
                        <a href="{{ url('/about') }}">
                            <i class="fa fa-angle-double-right"></i>{{ trans('welcome.About Us')}}
                        </a>
                    </li> --}}
                    <li>
                        <a href="{{ url('/careers') }}">
                            <i class="fa fa-angle-double-right"></i>{{ trans('welcome.Careers')}}
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/faq') }}">
                            <i class="fa fa-angle-double-right"></i>{{ trans('welcome.FAQ') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/termsandconditions') }}">
                            <i class="fa fa-angle-double-right"></i>{{ trans('welcome.Terms & Conditions') }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/privacyandpolicy') }}">
                            <i class="fa fa-angle-double-right"></i>{{ trans('welcome.Privacy and Policy') }}
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
                <h5>{{ trans('welcome.Contact Information') }}</h5>
                <ul class="list-unstyled quick-links">    
                    <li>
                        <strong>
                            {{ trans('welcome.Phone') }}
                        </strong><br>
                        123 123 123 

                    </li>
                    <li>
                        <strong>
                            {{ trans('welcome.Email') }}
                        </strong><br>
                        contact@techsouq.com
                    </li>
                </ul>    
               
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
               
                 <ul class="list-unstyled list-inline">
                    <li class="list-inline-item">
                        <a class="btn-floating btn-gplus mx-1">
                            <i class="fa fa-youtube-play"> </i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-fb mx-1">
                          <i class="fa fa-facebook-f"> </i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-tw mx-1">
                          <i class="fa fa-twitter"> </i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-tw mx-1">
                          <i class="fa fa-instagram"> </i>
                        </a>
                    </li>
                </ul>    
                <h5>{{ trans('welcome.Payment Method') }}</h5>
                <img src="{{asset('public/frontend/img/master-card.png')}}" class="img-fluid" style="max-width:210px;margin-bottom: 30px;"> 
            </div>
        </div>  
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-white">
                <p class="h6 float-left copywrite">© TECHNO SOUQ 2020. {{ trans('welcome.All Rights Reserved') }}
                </p>
                <p class="h6 float-right copywrite">{{ trans('welcome.Powered by') }} <a href="#" target="_blank">JeddahSoft</a>
                </p>
            </div>
            <hr>
        </div>  
    </div>
</section>


<button id="btn-top" title="Go to top" class="btn bg-primary shadow text-white">
    <i class="fa fa-arrow-up"></i>
</button>

<script src="//code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="//res.cloudinary.com/dxfq3iotg/raw/upload/v1565190285/Scripts/xzoom.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="//unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/masonry/4.2.2/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>

<script type="text/javascript" src="{{asset('public/frontend/js/jquery.countdown.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/frontend/js/bootnavbar.js')}}" ></script>
<script src="{{asset('public/frontend/js/jquery.magnify.js')}}"></script>
<script type="text/javascript" src="{{asset('public/frontend/magnific-popup/js/magnific-popup.js')}}"></script> 

@if(\Session::get('locale') == 'ar')
<script type="text/javascript" src="{{asset('public/frontend/js/ar/custom.js')}}"></script>
@else
<script type="text/javascript" src="{{asset('public/frontend/js/custom.js')}}"></script>
@endif

<script>
    @php $promotion = App\subcategory_promotions::where('disable_on_utc', '>', now())->first(); @endphp
    function makeTimer() {
        @if($promotion)
            var endTime = new Date('{{Carbon\Carbon::parse($promotion->disable_on_utc)}}');   
        @else
            var endTime = new Date();
        @endif       
        endTime = (Date.parse(endTime) / 1000);
        var now = new Date();
        now = (Date.parse(now) / 1000);
        var timeLeft = endTime - now;
        var days = Math.floor(timeLeft / 86400); 
        var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
        var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
        var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
        if (hours < "10") { hours = "0" + hours; }
        if (minutes < "10") { minutes = "0" + minutes; }
        if (seconds < "10") { seconds = "0" + seconds; }
        $("#days").html(days + "");
        $("#hours").html(hours + "");
        $("#minutes").html(minutes + "");
        $("#seconds").html(seconds + "");  
        $("#days1").html(days + "");
        $("#hours1").html(hours + "");
        $("#minutes1").html(minutes + "");
        $("#seconds1").html(seconds + "");       

    }

    setInterval(function() { makeTimer(); }, 1000);
    
    

</script>



 </body>
</html>