@extends('layouts.admin-app')
@section('content')

	<!-- <div class="row"> -->
        <div class="page-header">
            <h1>Dashboard</h1>
        </div><br>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="tile tile-primary">
                    <div class="tile-heading">Total Orders
                    </div>
                    <div class="tile-body">
                        <i class="fa fa-shopping-cart"></i>                    
                        <h2 class="pull-right">{{count($orders)}}</h2>
                    </div>
                    <div class="tile-footer">
                        <a href="{{url('/admin/orders')}}">View more...</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="tile tile-primary">
                    <div class="tile-heading">Total Sales
                    </div>
                    <div class="tile-body">
                        <i class="fa fa-credit-card"></i>
                         <h2 class="pull-right">SR {{$totalsale}}</h2>
                    </div>
                    <div class="tile-footer">
                        <a href="">View more...</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="tile tile-primary">
                    <div class="tile-heading">Total Customers
                    </div>
                    <div class="tile-body">
                        <i class="fa fa-user"></i>
                        <h2 class="pull-right">
                            {{count($customers)}}
                        </h2>
                    </div>
                    <div class="tile-footer">
                        <a href="{{ url('admin/customers') }}">View more...</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="tile tile-primary">
                    <div class="tile-heading">Total Products
                    </div>
                    <div class="tile-body">
                        <i class="fa fa-product-hunt"></i>
                        <div style="display:inline-block">
                            <h4 style="font-size: 15px;"> Total Active Products {{ $totalactiveproducts }} </h4>
                            <h4 style="font-size: 15px;"> Total Disable Products {{ $totaldisableproducts }}</h4>
                        </div>
                    </div>
                    <div class="tile-footer">
                        <a href="{{ url('admin/productsstatus') }}">View more...</a>
                    </div>
                </div>
            </div>
        </div>
        <h2>Recent Orders</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Customer Name</th>
                    <th>Order Status</th>
                    <th>Payment</th>
                    <th>Vat</th>
                    <th>Sub total</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                <tr>
                    <td>{{ $order->created_at->format('d/m/Y') }}</td>
                    {{-- <td>{{$order->user->name }}</td> --}}
                    <td style="max-width:200px;">{{$order->status}}</td>
                    <td style="max-width:200px;" >{{$order->payment}}</td>
                    <td><div class="MainTableNotes total7222">SR {{ $order->vat }}</div></td>
                    <td><div id="total7222">SR <?= $order->subtotal; ?></div> </td>
                    
                    <td><div class="MainTableNotes total7222">SR <?= $order->total; ?></div> </td>
                    <td class="text-right">
                        <div class="btn-group">
                            <a class="btn btn-default" href="{{route('orderitem.show',$order->id)}}">
                                <i class="icon-eye"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
               </tbody>
        </table>
        <div class="row">
            <div class="col-md-12" style="text-align:center;">
                <a class="btn btn-primary" href="">View All Orders</a>
            </div>
        </div>
        <h2>Recent Customers</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Joining Date</th>
                    {{-- <th>Customer Id</th> --}}
                    <th>Name</th>
                    <th>Mobile Number</th>
                    <th>Email </th>
                  </tr>
            </thead>
            <tbody>
            <?php
            foreach($customers as $customer)
            {
                ?>
                <tr>
                    <td><?= $customer->created_at; ?></td>
                    
                    <td class="gc_cell_left"><?= $customer->name; ?></td>
                    <td class="gc_cell_left"><?= $customer->phone; ?></td>
                    <td class="gc_cell_left"><?= $customer->email; ?></td>

                </tr>
                <?php
            }
            ?>

              
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12" style="text-align:center;">
                <a class="btn btn-primary" href="{{ url('admin/customers') }}">View All Customers</a>
            </div>
        </div>    
        <br>
        <br>
        <br>
@endsection
