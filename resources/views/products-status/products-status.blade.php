@extends('layouts.admin-app')
@section('content')
<div class="page-header">
    <h1>Products Status</h1>
</div>
<div class="row">
    <div class="col-md-6">
        <br>
        <div class="container demo">


            @foreach($categories as $category)


            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">


                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $category->id }}" aria-expanded="true" aria-controls="collapseOne">
                                <i class="more-less glyphicon glyphicon-plus" style="float: right;"></i>
                                {{ $category->cat_name }}
                                @php
                                $totalcategoriesproduct = App\products::where('categories_id', $category->id)->get()->count();
                                if(isset($totalcategoriesproduct)) { echo "(".$totalcategoriesproduct.")"; } else { echo 0; }
                                @endphp
                            </a>
                        </h4>
                    </div>
                    <div id="collapse{{ $category->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            @php
                            $subcategories = App\subcategories::where('parent_cat_id',$category->id)->get();
                            @endphp
                            @foreach($subcategories as $subcategory)
                            @php
                            $totalactiveproduct = App\products::where('sub_category_id', $subcategory->id)->where('status',1)->get()->count();
                            $totaldisableproducts = App\products::where('sub_category_id', $subcategory->id)->where('status',0)->get()->count();
                            @endphp
                            <label>{{ $subcategory->sub_cat_name }}</label>     <span style="margin-left: 25px">Enable <b>({{ $totalactiveproduct ?? 0 }})</b> Disable <b> ({{ $totaldisableproducts ?? 0 }})</b></span></label><br>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach



            </div><!-- panel-group -->


        </div><!-- container -->
    </div>
</div>
<br>




@endsection