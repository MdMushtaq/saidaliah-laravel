@extends('layouts.front-app')
@section('content')

<div id="category-section">
    <div class="container-fluid">
	    <div class="row">
	        <div class="col-md-12">

                @if (request()->segment(1) == 'new')
                    <h3 class="cat-title">{{ trans('allproductslist.NEW ARRIVAL') }}</h3>
                @elseif (request()->segment(1) == 'hot')
                    <h3 class="cat-title">{{ trans('allproductslist.HOT PRODUCTS') }}</h3>
                @else
                    <h3 class="cat-title">{{ trans('allproductslist.FEATURE CATEEGORIES') }}</h3>
                @endif
              
	        </div>
	    </div>
	</div>
</div>


<section class="mb-5">
	<div class="container-fluid cat-product">
		<div class="row">
            @foreach ($getproducts as $productlist)
            @php $getcategory = App\Categories::where(['id' => $productlist->categories_id , 'cat_status' => 1])->first(); @endphp
			<div class="col-lg-5-cols">
                <div class="product-block grid-v1">
                    <div class="hovereffect">

                        @if(\Session::get('locale') == 'ar')
                        @foreach (json_decode($productlist->arabic_image) as $key => $products)
                       
                        <a href="{{route('products.list',$productlist->id)}}">
                            @if($key == 0)
                            <img class="img-responsive w-100" height="{{$productlist->img_height}}" src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif

                            @if($key == 1)
                            <img class="img-responsive w-100 hover" src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif
                        </a>
                        @endforeach
                        @else
                        @foreach (json_decode($productlist->image) as $key => $products)
                        <a href="{{route('products.list',$productlist->id)}}">
                            @if($key == 0)
                            <img class="img-responsive w-100" height="{{$productlist->img_height}}" src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif

                            @if($key == 1)
                            <img class="img-responsive w-100 hover" src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif
                        </a>
                        @endforeach
                        @endif

                        <div class="metas clearfix">
                            <div class="title-wrapper">
                                @if(\Session::get('locale') == 'ar')
                                <h3 class="name">
                                    <a href="#" class="card-title">{{ $productlist->short_ar_name }}</a>
                                </h3>
                                @else
                                <h3 class="name">
                                    <a href="#" class="card-title">{{ $productlist->short_name }}</a>
                                </h3>
                                @endif

                                @if($productlist->discounted_price)

                                    <span class="sale-price" >{{ trans('welcome.SR') }} {{ $productlist->discounted_price }}</span>
                                    <span class="regular-price">SR {{$productlist->price}} </span>
                                    
                                @else
                                    <span class="sale-price" >{{ trans('welcome.SR') }} {{number_format($productlist->price)}} </span>
                                @endif
                            </div>
                        </div>
                        <div class="overlay"> 
                            <div class="social-icons">
                                <a href="#" class="card-hover">
                                    <button id="btnFA" class="btn btn-primary btn-xs">
                                        <i class="fa fa-shopping-cart"></i>
                                        {{ trans('welcome.ADD TO CART')}}

                                    </button>
                                </a>
                                <a href="#" data-tip="Add to Wishlist">
                                    <i class="fa fa-heart-o"></i>
                                </a>
                                <a href="" data-tip="Add to Cart">
                                    <i class="fa fa-share-alt"></i>
                                </a>
                            </div>    
                        </div>
                    </div>
                   
                </div>        
            </div>
          @endforeach
		</div>	
	</div>
	
</section>
@endsection