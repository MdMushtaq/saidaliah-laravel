@extends('layouts.front-app')
@section('content')

<div class="container">
    <div class="py-5 text-center">
        <h2> <p class="lead">{{ trans('checkout-form.Payment') }}</p></h2>
        <p class="lead">{{ trans('checkout-form.Need Some Informtion') }}</p>
    </div>
    <div class="stepwizard col-md-offset-3">
    	<div class="stepwizard-row setup-panel">
      		<div class="stepwizard-step">
        		<a href="#step-1" type="button" class="btn in-active-step btn-circle">1</a>
				<p>{{ trans('checkout-form.Account') }}</p>
      		</div>
      		<div class="stepwizard-step">
        		<a href="#step-2" type="button" class="btn btn-default btn-circle in-active-step" disabled="disabled">2</a>
				<p>{{ trans('checkout-form.Address') }}</p>
      		</div>
      		<div class="stepwizard-step">
        		<a href="#step-3" type="button" class="btn btn-default btn-circle btn-primary" disabled="disabled">3</a>
        		<p>{{ trans('checkout-form.Payment') }}</p>
      		</div>
    	</div>
  	</div>
    <div class="row">
        <div class="col-md-4 order-md-2 mt-4 mb-4 border-gray p-0">
            <div class="payment">
            	<h4 class="mt-3 ml-4">{{ trans('checkout-form.Cart - item') }}</h4>
            </div>	
			@foreach ($order->orderitems as $item)
				<div class="col-md-12 mt-4 mb-4 border-bottom-img">
					<div class="row product-container">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<div class="product-img">
							<img src="{{optional($item->product)->main_image}}" class="img-thumbnail rounded">
							</div>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<p class="product-text">
								{{-- @if(\Session::get('locale') == 'ar')
								{{$product->arabic_name }}
								@else
								{{$product->prod_name}}
								@endif --}}
							</p>
							<p>{{$item->product_quantity}} x  SR {{$item->product_unit_price}}</p>
							<p>{{ ucfirst($item->product_color) }}</p>

							@if($item->bar_code)
							<?php echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($item->bar_code, 'C39+',3,33,array(1,1,1), true) . '" alt="barcode" height="50" width="250"  />'; ?> 
							@endif
						</div>
					</div>	
				</div>
			@endforeach

    		<div class="payment">
            	<h4 class="mt-3 ml-4">Total</h4>
            </div>

    		<div class="col-md-12 mt-4 mb-4 border-bottom-img">
    			<div class="row product-container">
	        		<div class="col-md-12 col-sm-12 col-xs-12">
	            		<table class="table">
                			<tbody>
                				<tr>
                					<td> Subtotal:</td>
                					<td class="text-right">SR {{$order->subtotal}}</td>
                				</tr>
								<tr>
									<td>
										<strong>VAT:</strong>
									</td>
									<td class="text-right">
										<strong>SR {{$order->vat}}</strong>
									</td>
								</tr>
								<tr>
									<td>
										<strong>Shipping Price:</strong>
									</td>
									<td class="text-right">
										<strong>SR {{$order->shipping_price == '' ? 0 : $order->shipping_price}}</strong>
									</td>
								</tr>
  								<tr>
  									<td>
  										<strong>Total:</strong>
  									</td>
  									<td class="text-right">
  										<strong>SR {{ $order->total}}</strong>
  									</td>
								  </tr>
								  
                			</tbody>
						</table>
						
					
	        		</div>
	        	</div>	
    		</div>
        </div>

        <div class="col-md-8 order-md-1 mt-4 mb-4">
            <div class="row">
                <div class="col-md-6">
					<h4 class="mt-3 ">Billing Address</h4>
					<strong>Name</strong>
					<br>
					<small>Email</small>
					<br>
					<small>Mobile</small>
				</div>
				
				<div class="col-md-6">
					<h4 class="mt-3">Shipping Address</h4>
					<strong>Name</strong>
					<br>
					<small>Email</small>
					<br>
					<small>Mobile</small>
                </div>
            </div>
        	{{-- <div class="row mt-3">
                <div class="col-md-6">
                    <div class="payment">
                        <h4 class="mt-3 ml-4">Payment Methods</h4>
                    </div>
                    <div class="shipping">
                        <p>Bank Transfer</p>
                        <p>Cash on Delivery</p>
                        <p>
                            <img class="img-fluid cc-img payment-img" src="https://saidaliah.me/9/uploads/offer/visa-logo.jpg" />
                        </p>
                    </div>
                    <div class="custom-control custom-checkbox mt-3 mb-3">
                        <input type="checkbox" class="custom-control-input" id="agree-terms">
                        <label class="custom-control-label" for="agree-terms">
                            <a href="#" target="_blank">I have read and agree to the Terms & Conditions</a>
                        </label>
					</div>
					
                </div>
                
			</div> --}}
			
		</div>
		
	</div>
	<p id="demo"></p>
	@php $user = $order->user;@endphp
	<script type="text/javascript" src="https://goSellJSLib.b-cdn.net/v1.6.0/js/gosell.js"></script>
	<div id="root"></div>

	<div class="row">
		<div class="col-md-8"></div>
		<div class="col-md-4">
			<button id="openLightBox" onclick="goSell.openLightBox()" class="btn btn-success form-control" style="background:#39509c;font-size: 25px;height:60px;">Checkout</button>
			
		</div>
	</div>
	<br>
	
	
<script>

	goSell.config({
	"containerID": "root",
	"gateway": {
		"publicKey": "pk_test_gw06d87941vubZqis5zcNQTl",
		"merchantId": 3480289,
		"language": "en",
		"contactInfo": true,
		"supportedCurrencies": "all",
		"supportedPaymentMethods": "all",
		"saveCardOption": false,
		"customerCards": true,
		"notifications": "standard",
		"callback": function(response){ console.log('response', response); },
		"onClose": function(response){ console.log("onClose Event"); },
		"backgroundImg": {
			"url": "imgURL",
			"opacity": "0.5"
		},
		"labels": {
			"cardNumber": "Card Number",
			"expirationDate": "MM/YY",
			"cvv": "CVV",
			"cardHolder": "Name on Card",
			"actionButton": "Pay"
		},
		"style": {
			"base": {
				"color": "#535353",
				"lineHeight": "18px",
				"fontFamily": "sans-serif",
				"fontSmoothing": "antialiased",
				"fontSize": "16px",
				"::placeholder": {
				"color": "rgba(0, 0, 0, 0.26)",
				"fontSize": "15px"
				}
			},
			"invalid": {
				"color": "red",
				"iconColor": "#fa755a "
			}
		}
	},
	"customer": {
		
		"first_name": "{{$user->name}}",
		"email": "{{$user->email}}",
		"phone": {
			"number": "{{$user->phone}}"
		}
	},
	"order": {
		"amount": "{{$order->total}}",
		"currency": "SAR",
		"items": @json($order->items),
		"shipping": "{{$order->shipping_price == '' ? 0 : $order->shipping_price}}",
		"taxes": null
	},
	"transaction": {
		"mode": "charge",
		"charge": {
			"saveCard": false,
			"threeDSecure": true,
			"statement_descriptor": "Sample",
			"reference": {
				"transaction": "{{$order->id}}",
				"order": "{{$order->id}}"
			},
			"metadata": {},
			"receipt": {
				"email": false,
				"sms": true
			},
			"redirect": "{{route('redirect.success')}}",
			"post": "{{route('tap.post')}}",
		}
	}
});
</script>
</div>
@endsection