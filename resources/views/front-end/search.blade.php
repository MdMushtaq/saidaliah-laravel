@extends('layouts.front-app')
@section('content')
<section class="mb-5">
	<div class="container-fluid cat-product">
		<div class="row">
            @foreach ($searchproducts as $productlist)
            @php $getcategory = App\Categories::where(['id' => $productlist->categories_id , 'cat_status' => 1])->first(); @endphp
			<div class="col-lg-5-cols">
                <div class="product-block grid-v1">
                    <div class="hovereffect">

                        @if(\Session::get('locale') == 'ar')
                        @foreach (json_decode($productlist->arabic_image) as $key => $products)
                       
                        <a href="{{route('products.list',$productlist->id)}}">
                            @if($key == 0)
                            <img class="img-responsive w-100" height="{{$productlist->img_height}}" src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif

                            @if($key == 1)
                            <img class="img-responsive w-100 hover" src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif
                        </a>
                        @endforeach
                        @else
                        @foreach (json_decode($productlist->image) as $key => $products)
                        <a href="{{route('products.list',$productlist->id)}}">
                            @if($key == 0)
                            <img class="img-responsive w-100" height="{{$productlist->img_height}}" src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif

                            @if($key == 1)
                            <img class="img-responsive w-100 hover" src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif
                        </a>
                        @endforeach
                        @endif

                        <div class="metas clearfix">
                            <div class="title-wrapper">
                                @if(\Session::get('locale') == 'ar')
                                <h3 class="name">
                                    <a href="#" class="card-title">{{ $productlist->arabic_name }}</a>
                                </h3>
                                @else
                                <h3 class="name">
                                    <a href="#" class="card-title">{{ $productlist->prod_name }}</a>
                                </h3>
                                @endif

                                @if($productlist->discounted_price)

                                    <span class="sale-price" >{{ trans('welcome.SR') }} {{ $productlist->discounted_price }}</span>
                                    <span class="regular-price">SR {{$productlist->price}} </span>
                                    
                                @else
                                    <span class="sale-price" >{{ trans('welcome.SR') }} {{number_format($productlist->price)}} </span>
                                @endif
                            </div>
                        </div>
                        <div class="overlay"> 
                            <div class="social-icons">
                                <a href="#" class="card-hover">
                                    <button id="btnFA" class="btn btn-primary btn-xs">
                                        <i class="fa fa-shopping-cart"></i>
                                        {{ trans('welcome.ADD TO CART')}}

                                    </button>
                                </a>
                                <a href="#" data-tip="Add to Wishlist">
                                    <i class="fa fa-heart-o"></i>
                                </a>
                                <a href="" data-tip="Add to Cart">
                                    <i class="fa fa-share-alt"></i>
                                </a>
                            </div>    
                        </div>
                    </div>
                   
                </div>        
            </div>
          @endforeach
		</div>	
	</div>
	
</section>

<!-- News letter -->
<section id="newsletter">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <h3>{{ trans('welcome.Get The Latest News') }}</h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <form action="{{ url('/subscribe') }}"  method="POST">
                    @csrf
                    <input type="email" name="email" placeholder="{{ trans('welcome.Enter your Email') }}">
                    <input type="submit" value="{{ trans('welcome.Subscribe') }}">
                </form>
            </div>
        </div>
    </div>
</section>
@if($message = Session::get('successfulsubsciption'))
<script>
    alert("Email Subscribed for Newsletter");
</script>
@endif
@endsection