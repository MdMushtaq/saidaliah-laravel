@extends('layouts.front-app')
@section('content')

<h3>
  Thanky You
</h3>
<script
      type="text/javascript"
      src="https://goSellJSLib.b-cdn.net/v1.6.0/js/gosell.js"
    ></script>

    <div id="root"></div>
    <script>
       goSell.showResult({
           callback({callback}) {
             console.log(callback, callback.id);
             $.post('{{route('tap.charge')}}', {
               '_token': '{{csrf_token()}}',
               id: callback.id
             })
         }
      });
    </script>
    
@endsection