@extends('layouts.front-app')

@section('content')

<div id="category-section">
    <div class="container-fluid">
	    <div class="row">
	        <div class="col-md-12">
                 @php $categories = App\Categories::where('id',$subcategories->parent_cat_id)->first();@endphp

                <h3 class="cat-title">
                    @if(\Session::get('locale') == 'ar')
                    {{ strtoupper($categories->cat_arabic_name) }} -- {{ strtoupper($subcategories->sub_cat_arabic_name)}}
                    @else
                    {{ strtoupper($categories->cat_name) }} -- {{ strtoupper($subcategories->sub_cat_name)}}
                    @endif
                </h3>

	        </div>
	    </div>
	</div>
</div>


<section class="mb-5">
	<div class="container-fluid cat-product">
		<div class="row">
            @foreach ($getproducts as $productlist)
			<div class="col-lg-5-cols">
                <div class="product-block grid-v1">
                    <div class="hovereffect">
                       @if(\Session::get('locale') == 'ar')
                        @foreach (json_decode($productlist->arabic_image) as $key => $products)
                        <a href="{{route('products.list',$productlist->id)}}">
                            @if($key == 0)
                            <img class="img-responsive w-100" height="{{$productlist->img_height}}" src="{{asset('public/images/products/'.$categories->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif

                            @if($key == 1)
                            <img class="img-responsive w-100 hover" src="{{asset('public/images/products/'.$categories->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif
                        </a>
                        @endforeach
                        @else
                        @foreach (json_decode($productlist->image) as $key => $products)
                        <a href="{{route('products.list',$productlist->id)}}">
                            @if($key == 0)
                            <img class="img-responsive w-100" height="{{$productlist->img_height}}" src="{{asset('public/images/products/'.$categories->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif

                            @if($key == 1)
                            <img class="img-responsive w-100 hover" src="{{asset('public/images/products/'.$categories->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif
                        </a>
                        @endforeach
                        @endif

                        <div class="metas clearfix">
                            <div class="title-wrapper">
                                <h3 class="name">
                                    <a href="#" class="card-title">
                                        @if(\Session::get('locale') == 'ar')
                                        {{ucfirst($productlist->arabic_name)}}
                                        @else
                                        {{ucfirst($productlist->prod_name)}}
                                        @endif
                                    </a>
                                </h3>
                                @if($productlist->discounted_price)

                                <span class="sale-price" >{{ trans('welcome.SR') }} {{ $productlist->discounted_price }}</span>
                                <span class="regular-price">SR {{$productlist->price}}</span>
                                
                                @else
                                <span class="sale-price" >{{ trans('welcome.SR') }} {{number_format($productlist->price)}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="overlay">  
                            <form action="{{route('cart.added',$productlist->id)}}" method="POST">
                            @csrf
                            @php $colorattributes = App\Productattributes::where('productid',$productlist->id )->first(); @endphp
                            <input type="hidden" name="product_color" value="{{json_encode(['id' => $colorattributes->id, 'name' => strtolower($colorattributes->colourname)])}}">
                            
                            <input type="hidden" name="qty" value="1">
                                <div class="social-icons">
                                    <button id="btnFA" type="submit" class="btn btn-primary btn-xs card-hover">
                                        <i class="fa fa-shopping-cart"></i>
                                        {{ trans('subcategoryproduct.ADD TO CART') }}
                                    </button>

                                    <a href="#" data-tip="Add to Wishlist">
                                        <i class="fa fa-heart-o"></i>
                                    </a>
                                    <a href="" data-tip="Add to Cart">
                                        <i class="fa fa-share-alt"></i>
                                    </a>
                                </div>  
                             </form>  
                        </div>
                    </div>
                   
                </div>        
            </div>
          @endforeach
		</div>	
	</div>
	
</section>



 

@endsection