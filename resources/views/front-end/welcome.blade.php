@extends('layouts.front-app')

@section('content')

<div class="container-fluid p-0">
    <div class="row">
        <div class="col-md-12">
            <!-- Main Slider -->
            <div class="position-relative">
                <div id="main-slider" class="carousel slide {{\Session::get('locale') == 'ar' ? 'rtl' : ''}}" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                        <li data-target="#main-slider" data-slide-to="1"></li>
                        <li data-target="#main-slider" data-slide-to="2"></li>
                        <li data-target="#main-slider" data-slide-to="3"></li>
                        <li data-target="#main-slider" data-slide-to="4"></li>
                        
                    </ul>
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        @php
                        $count = 1;
                        @endphp
                        @foreach($banners as $banner)
                        <div class="carousel-item {{ $count == 1 ? 'active' : '' }}">
                            <a href="{{$banner->banner_link}}">
                            @if(\Session::get('locale') == 'ar')
                            
                                <img src="{{asset('public/images/banners/'.$banner->banner_arabic_img) }}" class="img-fluid" alt="">
                          
                            @else
                            <img src="{{asset('public/images/banners/'.$banner->banner_img) }}" class="img-fluid" alt="">
                            @endif
                         </a>
                        </div>
                        @php
                        $count++;
                        @endphp
                        @endforeach
                        <!-- Brand Logos Slider -->
                   
            <!-- End Brand Logos Slider -->
                    </div>
                </div>
            </div>
            <!-- End Main Slider --> 
        </div>
    </div>
</div>

{{-- brand section --}}
<div class="container-fluid mt-custom">
    <div class="row">
        <!-- Brand Logos Slider -->
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="brands_slider_container">
                <div id="brands-slider" class="owl-carousel owl-theme brands_slider">
                   
                    <div class="item">
                        <div class="brands_item d-flex flex-column justify-content-center img-circle">
                            <img src="{{asset('public/frontend/img/brands/1000Products.svg')}}" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="brands_item d-flex flex-column justify-content-center img-circle">
                            <img src="{{asset('public/frontend/img/brands/quickdelivery.svg')}}" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="brands_item d-flex flex-column justify-content-center img-circle">
                            <img src="{{asset('public/frontend/img/brands/securepayment.svg')}}" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="brands_item d-flex flex-column justify-content-center img-circle">
                            <img src="{{asset('public/frontend/img/brands/stylishdesign.svg')}}" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="brands_item d-flex flex-column justify-content-center img-circle">
                            <img src="{{asset('public/frontend/img/brands/topquality.svg')}}" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="brands_item d-flex flex-column justify-content-center img-circle">
                            <img src="{{asset('public/frontend/img/brands/1000Products.svg')}}" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="brands_item d-flex flex-column justify-content-center img-circle">
                            <img src="{{asset('public/frontend/img/brands/quickdelivery.svg')}}" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="brands_item d-flex flex-column justify-content-center img-circle">
                            <img src="{{asset('public/frontend/img/brands/securepayment.svg')}}" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="brands_item d-flex flex-column justify-content-center img-circle">
                            <img src="{{asset('public/frontend/img/brands/stylishdesign.svg')}}" alt="">
                        </div>
                    </div>
                    <div class="item">
                        <div class="brands_item d-flex flex-column justify-content-center img-circle">
                            <img src="{{asset('public/frontend/img/brands/topquality.svg')}}" alt="">
                        </div>
                    </div>
                   
                </div>
                <!-- Brands Slider Navigation -->
            </div>
        </div>
        <!-- End Brand Logos Slider -->  
    </div>
</div>

 <!-- Wrapper Container -->
<div id="wrapper-container">
    <!-- New Arrival Products -->
    @php
    
    $getnewarrival = App\products::where(['status' => 1 , 'newarrival' => 'on' ])->orderBy('created_at','desc')->limit(10)->get();
    $gethotproducts = App\products::where(['status' => 1 , 'hotproducts' => 'on' ])->orderBy('created_at','desc')->limit(10)->get();
    $getfeaturedproducts = App\products::where(['status' => 1 , 'featuredproducts' => 'on' ])->orderBy('created_at','desc')->limit(10)->get();  
   @endphp
    <div class="container-fluid top-special">
        <h3 class="title">{{ trans('welcome.NEW ARRIVAL') }}</h3> <span>
            <a href="{{route('new.arrival')}}" class="viewmore" style="z-index: 9999999;">{{ trans('welcome.View More') }} <i class="fa fa-plus"></i></a>
        </span>
       
        <div class="row special-products mt-4 ">     
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card border-0">
                    <div class="card-body p-0">
                        <div id="new-arrival" class="owl-carousel owl-theme">
                            @foreach ($getnewarrival  as $productlist)
                                @php $getcategory = App\Categories::where(['id' => $productlist->categories_id , 'cat_status' => 1])->first(); @endphp
                                <div class="item">
                                    <div class="h-100 p-2">
                                        <a href="{{route('products.list',$productlist->id)}}" class="card-hover">
                                            <div class="card">
                                                @foreach (json_decode($productlist->image) as $key => $products)
                                                    @if($key == 0)
                                                    <img class="card-img-top" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}" style="width:100%">
                                                    @endif
                                                @endforeach
                                                <div class="card-body">
                                                    <h4 class="card-title">
                                                        @if(\Session::get('locale') == 'ar')
                                                        {{ ucfirst($getcategory->cat_arabic_name) }}
                                                        @else
                                                        {{ ucfirst($getcategory->cat_name) }}
                                                        @endif

                                                        @if($productlist->discounted_price)

                                                        <span class="sale-price" >{{ trans('welcome.SR') }} {{ $productlist->discounted_price }}</span>
                                                        <span class="regular-price">SR {{$productlist->price}}</span>
                                                        
                                                        @else
                                                        <span class="sale-price" >{{ trans('welcome.SR') }} {{number_format($productlist->price)}}</span>
                                                        @endif

                                                    </h4>
                                                </div>
                                            </div> 
                                        </a>
                                    </div>
                                </div>
                               
                            @endforeach
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Arrival Products -->

    <!-- New Hot Products -->
    <div class="container-fluid top-special">
        <h3 class="title">{{ trans('welcome.HOT PRODUCTS') }}</h3>
        <span>
            <a href="{{route('hot.product')}}" class="viewmore">{{ trans('welcome.View More') }}
                <i class="fa fa-plus"></i>
            </a>
        </span>
       
        <div class="row special-products mt-4 ">     
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card border-0">
                    <div class="card-body p-0">
                        <div id="hot-products" class="owl-carousel owl-theme">
                            @foreach ($gethotproducts as $productlist)
                            @php $getcategory = App\Categories::where(['id' => $productlist->categories_id , 'cat_status' => 1])->first(); @endphp
                                <div class="item">
                                    <div class="h-100 p-2">
                                        <a href="{{route('products.list',$productlist->id)}}" class="card-hover">
                                            <div class="card">
                                                @if(\Session::get('locale') == 'ar')
                                                @foreach (json_decode($productlist->arabic_image) as $key => $products)
                                                @if($key == 0)
                                                <img class="card-img-top" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}" style="width:100%">
                                                @endif
                                                @endforeach
                                                @else
                                                @foreach (json_decode($productlist->image) as $key => $products)
                                                @if($key == 0)
                                                <img class="card-img-top" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}" style="width:100%">
                                                @endif
                                                @endforeach
                                                @endif
                                                <div class="card-body">
                                                    <h4 class="card-title">
                                                        @if(\Session::get('locale') == 'ar')
                                                        {{ ucfirst($getcategory->cat_arabic_name) }}
                                                        @else
                                                        {{ ucfirst($getcategory->cat_name) }}
                                                        @endif

                                                        @if($productlist->discounted_price)

                                                        <span class="sale-price" >{{ trans('welcome.SR') }} {{ $productlist->discounted_price }}</span>
                                                        <span class="regular-price">SR {{$productlist->price}}</span>
                                                        
                                                        @else
                                                        <span class="sale-price" >{{ trans('welcome.SR') }} {{number_format($productlist->price)}}</span>
                                                        @endif

                                                    </h4>
                                                </div>
                                            </div> 
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Hot Products -->

    <!-- New Feature Products -->
    <div class="container-fluid top-special">
        <h3 class="title">{{ trans('welcome.FEATURE CATEEGORIES') }}</h3>
        <span>
            <a href="{{route('feature.product')}}" class="viewmore">{{ trans('welcome.View More') }}
                <i class="fa fa-plus"></i>
            </a>
        </span>
        <div class="row special-products mt-4 mb-4">     
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card border-0">
                    <div class="card-body p-0">
                        <div id="feature-products" class="owl-carousel owl-theme">
                            @foreach ($getfeaturedproducts as $productlist)
                            @php $getcategory = App\Categories::where(['id' => $productlist->categories_id , 'cat_status' => 1])->first(); @endphp
                                <div class="item">
                                    <div class="h-100 p-2">
                                        <a href="{{route('products.list',$productlist->id)}}" class="card-hover">
                                            <div class="card">
                                                @if(\Session::get('locale') == 'ar')
                                                @foreach (json_decode($productlist->arabic_image) as $key => $products)
                                                @if($key == 0)
                                                <img class="card-img-top" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}" style="width:100%">
                                                @endif
                                                @endforeach
                                                @else
                                                @foreach (json_decode($productlist->image) as $key => $products)
                                                @if($key == 0)
                                                <img class="card-img-top" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}" style="width:100%">
                                                @endif
                                                @endforeach
                                                @endif
                                                <div class="card-body">
                                                    <h4 class="card-title">
                                                        @if(\Session::get('locale') == 'ar')
                                                        {{ ucfirst($getcategory->cat_arabic_name) }}
                                                        @else
                                                        {{ ucfirst($getcategory->cat_name) }}
                                                        @endif
                                                       

                                                        @if($productlist->discounted_price)

                                                        <span class="sale-price" >{{ trans('welcome.SR') }} {{ $productlist->discounted_price }}</span>
                                                        <span class="regular-price">SR {{$productlist->price}}</span>
                                                        
                                                        @else
                                                        <span class="sale-price" >{{ trans('welcome.SR') }} {{number_format($productlist->price)}}</span>
                                                        @endif

                                                    </h4>
                                                </div>
                                            </div> 
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Feature Products -->


</div>
<!-- End Wrapper Container -->

<!-- News letter -->
<section id="newsletter">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <h3>{{ trans('welcome.Get The Latest News') }}</h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <form action="{{ url('/subscribe') }}"  method="POST">
                    @csrf
                    <input type="email" name="email" placeholder="{{ trans('welcome.Enter your Email') }}">
                    <input type="submit" value="{{ trans('welcome.Subscribe') }}">
                </form>
            </div>
        </div>
    </div>
</section>
@if($message = Session::get('successfulsubsciption'))
<script>
    alert("Email Subscribed for Newsletter");
</script>
@endif
@endsection