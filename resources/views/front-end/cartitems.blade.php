@extends('layouts.front-app')
@section('content')

<div class="container">
    <br>
    <h3>Order Items</h3>
<table class="table">
    <tbody class="table table-striped">
        @foreach ($itemorder as $order)
            @foreach ($order->orderitems as $item)
                <tr>
                    <td><strong>{{$item->product_name}}</strong> <br></td>
                    <td>{{$item->product_color != '' ? $item->product_color : ''}}</td>
                    <td>
                        <div style="font-size:11px; color:#bbb;">({{$item->product_quantity}}  ×  {{$item->product_unit_price}})</div>SR {{ number_format($item->product_price, 2)}}
                    </td>
                </tr>
            @endforeach

        @endforeach
       
    </tbody>
   
    <tbody class="orderTotals">
        <tr>
            <td colspan="2">Subtotal</td>
            <td>SR {{ number_format($order->subtotal, 2)}}</td>
        </tr>
      	<tr>
            <td colspan="2" class="cartSummaryTotalsKey tex">VAT :</td>
            <td class="cartSummaryTotalsValue ">SR {{number_format($order->vat, 2)}}</td>
        </tr>
        <tr>
            <td colspan="2" >Shipping Price :</td>
            <td class="cartSummaryTotalsValue ">SR {{number_format($order->shipping_price, 2)}}</td>
        </tr>
        <tr>
        <td colspan="2">
            <div style="font-size:17px;"><b>Total :</b></div>
        </td>
            <td colspan="2">
                <div style="font-size:17px;"><b>SR {{ number_format($order->total, 2) }}</b></div>
            </td>
        </tr>
    </tbody>
</table>
</div>

@endsection