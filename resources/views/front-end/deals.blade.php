@extends('layouts.front-app')
@section('content')
<!--Product wise products -->
<!--<h1>Products</h1>-->
<section class="mb-5">
    <div class="container-fluid cat-product">
        <div class="row">
            @php $promotion = App\subcategory_promotions::where('disable_on_utc', '>', now())->first();@endphp
            @if($promotion)
            @php  $product = App\products::where('all_offers',$promotion->id)->get(); @endphp
                @foreach ($product as $productlist)
                @php $getcategory = App\Categories::where(['id' => $productlist->categories_id , 'cat_status' => 1])->first(); @endphp
                    <div class="col-lg-5-cols">
                        <div class="product-block grid-v1">
                            <div class="hovereffect">
                                @foreach (json_decode($productlist->image) as $key => $products)
                                <a href="{{route('products.list',$productlist->id)}}">
                                    @if($key == 0)
                                    <img class="img-responsive w-100" height="{{$productlist->img_height}}" src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                                    @endif

                                    @if($key == 1)
                                    <img class="img-responsive w-100 hover" src="{{asset('public/images/products/'.$getcategory->cat_name.'/'.$products)}}" alt="{{ $productlist->prod_name}}">
                                    @endif
                                </a>
                                @endforeach
                                <div class="metas clearfix">
                                    <div class="title-wrapper">
                                        <h3 class="name">
                                            @if(\Session::get('locale') == 'ar')
                                            <a href="#" class="card-title">{{$productlist->arabic_name}}</a>
                                            @else
                                            <a href="#" class="card-title">{{$productlist->prod_name}}</a>
                                            @endif
                                        </h3>

                                    
                                        @if($productlist->discounted_price)

                                        <span class="sale-price" >{{ trans('welcome.SR') }} {{ $productlist->discounted_price }}</span>
                                        <span class="regular-price">{{ trans('deals.SR') }} {{$productlist->price}}</span>
                                        
                                        @else
                                        <span class="sale-price" >{{ trans('welcome.SR') }} {{number_format($productlist->price)}}</span>
                                        @endif

                                    </div>
                                </div>
                                <div class="overlay">
                                    <div class="social-icons">
                                        <a href="#" class="card-hover">
                                            <button id="btnFA" class="btn btn-primary btn-xs">
                                                <i class="fa fa-shopping-cart"></i>
                                                {{ trans('deals.ADD TO CART') }}
                                            </button>
                                        </a>
                                        <a href="#" data-tip="Add to Wishlist">
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                        <a href="" data-tip="Add to Cart">
                                            <i class="fa fa-share-alt"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                @endforeach
            @else
            <div class="col-md-12 text-center">
                <h1>{{ trans('deals.There is no Deal') }}</h1>
            </div>
            @endif
        </div>
    </div>

</section>
<!--Product wise products -->



@endsection