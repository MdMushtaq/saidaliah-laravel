<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderitem extends Model
{
    //
    protected $table = 'orderitem';

    public function product()
    {
        return $this->belongsTo(products::class);
    }

    public function getBarCodeAttribute()
    {
        $barcode = Productattributes::where('id',$this->color_id)->first();
        if(isset($barcode))
        {
            return $barcode->sku;
        }
        return null
       ;
    }
}


