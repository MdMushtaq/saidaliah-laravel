<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productattributes extends Model
{
    //
    protected $table = 'productattributes';

    public function order_items()
    {
        return $this->hasMany(Orderitem::class, 'color_id');
    }
}
