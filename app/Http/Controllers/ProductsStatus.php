<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\products;

class ProductsStatus extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //
    public function index()
    {
        $categories = Categories::all();
        return view('products-status.products-status', compact('categories'));
    }
}
