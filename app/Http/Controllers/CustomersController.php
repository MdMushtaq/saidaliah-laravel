<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Countries;

class CustomersController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index()
    {
        $data["customers"] = User::where('status', 1)->get();
        return view('customer.customers', $data);
    }

    public function addform()
    {
        $data["countries"] = Countries::all();
        return view('customer.customer-form', $data);
    }

    public function addformpost(Request $request)
    {
            $this->validate($request, [
                'name'   => 'required',
                // 'lastname'   => 'required',
                'gender'   => 'required',
                'phone'   => 'required',
                'email'   => 'required|email|unique:users',
                'password' => 'required|min:6|confirmed'
            ]);

            $user = new User;
            $user->name = $request->name;
            // $user->lastname = $request->lastname;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->gender = $request->gender;
            $user->address = $request->address;
            $user->country = $request->country;
            $user->city = $request->city;
            $user->password = Hash::make($request->password);
            $user->status = $request->status;
            $user->save();

            $customerid = $user::max('id');
            $customeridc = "00".$customerid;

            $user::where('id', $customerid)->update(["customerid"=>$customeridc]);

            return redirect('admin/customers');
        
    }

    public function editform($customerid)
    {
        $data["user"] = User::find($customerid);
        $data["customerid"] = $customerid;
        $data["countries"] = Countries::all();
        return view('customer.customeredit', $data);
    }

    public function editformpost(Request $request)
    {
        if(!empty($request->password))
        {

            if($request->password != $request->confirm)
            {
                return back()->with('danger','Password not matched');
            }
            else
            {
                $country = Countries::where('id', $request->country)->first();

                $updateColumns = array(
                    'name' => $request->name,
                    // 'lastname' => $request->lastname,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'gender' => $request->gender,
                    'password' => bcrypt($request->password),
                    'country' => $request->country,
               
                    'city' => $request->city,
                    'status' => $request->status
                );

                $country = Countries::where('id', $request->country)->first();

                User::where('id', $request->customerid)->update($updateColumns);

                return redirect('admin/customers');
            }
        }
        else
        {
            $country = Countries::where('id', $request->country)->first();

            $updateColumns = array(
                'name' => $request->name,
                // 'lastname' => $request->lastname,
                'email' => $request->email,
                'phone' => $request->phone,
                'gender' => $request->gender,
                'country' => $request->country,
                'city' => $request->city,
                'status' => $request->status
            );

            User::where('id', $request->customerid)->update($updateColumns);

//            return back();
            return redirect('admin/customers');
        }
    }

    public function customersarchive()
    {
        $data["customers"] = User::where('status', 2)->get();
        return view('customer.customersarchive', $data);
    }

    public function customerssuspend()
    {
        $data["customers"] = User::where('status', 0)->get();
        return view('customer.customerssuspend', $data);
    }

    public function exporttoexcelcustomers(Request $request)
    {
        $customerids = $request->customerids;

        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=customersreport.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo "<table>";
        echo "<tr><th>S. No.</th><th>Joining Date</th><th>Name</th><th>Mobile Number</th><th>Email </th><th>Country</th><th>City</th>";

        $count = 1;
        foreach($customerids as $customerid)
        {
            $customer = User::where('status', 1)->where('id',$customerid)->first();

            echo "<tr>";
            echo "<td>".$count."</td>";
            echo "<td>".$customer->created_at->format('Y-m-d')."</td>";
            echo "<td>".$customer->name."</td>";
            echo "<td>".$customer->phone."</td>";
            echo "<td>".$customer->email."</td>";
            echo "<td>".$customer->country."</td>";
            echo "<td>".$customer->city."</td>";
            echo "</tr>";
            $count++;
        }
        echo "</table>";
    }
}
