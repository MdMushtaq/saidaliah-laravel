<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Orders;
use App\products;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data["customers"] = User::all();
        $orders = Orders::with('orderitems')->where('payment','Success')->orderBy('id', 'DESC')->get();
        $totalsale = Orders::where('status','Delivered')->sum('total');

        $totalactiveproducts = products::where('status',1)->get()->count();
        $totaldisableproducts = products::where('status',0)->get()->count();

        return view('admin', $data,compact('orders', 'totalsale', 'totalactiveproducts', 'totaldisableproducts'));
    }
}
