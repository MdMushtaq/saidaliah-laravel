<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\product_promotions;
use App\category_promotions;
use App\subcategory_promotions;
use App\brand_promotions;
use App\Brands;
// use Session;
use App\Categories;
use App\subcategories;
use App\products;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function product_offers()
    {
        $data["products_wise_discounts"] = product_promotions::all();
        return view('promotion.product_wise_discount', $data);
    }

     function add_product_wise_offer()
     {
         $data["categories"] = Categories::all();
         $data["products"] = products::all();
         return view('promotion.add_product_wise_offer', $data);
     }

    function product_offerspost(Request $request)
    {
        $promotion = products::where('promotion_type','product')->orWhere('promotion_type', 'subcategory')->first();
        if(gettype($promotion) == "object")
        {
            return back()->with('danger','Only one promotion can be added please remove promotion to add new promotion');
        }
        else
        {
            $productpromotion = new product_promotions;
            $productpromotion->offer_name = $request->offer_name;
            $productpromotion->enable_on_utc = $request->start_date;
            $productpromotion->disable_on_utc = $request->end_date;
            $productpromotion->reduction_type = $request->reduction_type;
            $productpromotion->percentage = $request->reduction_amount;
            $productpromotion->status = $request->enabled_1;

            $productpromotion->category_id = $request->primary_category;
            $productpromotion->subcategory_id = $request->secondary_category;

            $productpromotion->save();

            $products = new products;

            $productpromotionid = $productpromotion::max('id');

            $totalnumberofproducts = $request->numberofproducts;

            for($a = 1; $a <= $totalnumberofproducts; $a++)
            {
                $b = "product".$a;
                $productid =  $request->$b;

                $updateColumns = array(
                    'all_offers'=>$productpromotionid,
                    'promotion_type'=>'product'
                );

                $products::where('id', $productid)->update($updateColumns);
            }
            return redirect('admin/product_offers')->with('success','Successfully created Product Offers');
        }


    }

    function product_offersedit($id)
    {
        $data["categories"] = Categories::all();
        $data["productwisediscount"] = product_promotions::where('id', $id)->first();
        $data["promotionselectedproducts"] = products::where('all_offers', $data["productwisediscount"]->id)->where("promotion_type","product")->get();
        return view("promotion.editproductwisediscount", $data);
    }

    function product_formpostedit(Request $request)
    {
        $productwisediscountid = $request->productwisediscountid;

        $updateColumns = array(
            'offer_name'=>$request->offer_name,
            'enable_on_utc'=>$request->start_date,
            'disable_on_utc'=>$request->end_date,
            'reduction_type'=>$request->reduction_type,
            'percentage'=>$request->reduction_amount,
            'status'=>$request->enabled_1
        );

        $product_promotions = new product_promotions;
        $product_promotions::where('id', $productwisediscountid)->update($updateColumns);


        $products = new products;

        $totalnumberofproducts = $request->numberofproducts;

        for($a = 1; $a <= $totalnumberofproducts; $a++)
        {
            $b = "product".$a;
            $productid =  $request->$b;

            $updateColumns = array(
                'all_offers'=>$productwisediscountid,
                'promotion_type'=>'product'
            );

            $products::where('id', $productid)->update($updateColumns);
        }


        return redirect('admin/product_offers')->with('success','Successfully Update Product Offers');
    }

    function product_offers_delete($id)
    {
        $product_promotions = product_promotions::find($id);


        $updateColumns = array(
            'all_offers'=>'',
            'promotion_type'=>''
        );

        $products = new products;

        $products::where('all_offers', $id)->where('promotion_type','product')->update($updateColumns);

        $product_promotions->delete();

        return back()->with('danger','Successfully Deleted Product Offers');
    }

    // categories
    function category_wise_offers()
    {
        return view('promotion.category_wise_offer');
    }
    
    // sub categories
    

    function sub_category_offers()
    {
        $data["category_wise_discounts"] = subcategory_promotions::all();
        return view('promotion.sub_categories_wise_discount', $data);
    }

    function sub_category_wise_offers()
    {
        $data["categories"] = Categories::all();
        return view('promotion.sub_category_wise_offersform', $data);
    }
    function sub_category_offersedit($categorywisediscountid) //work area
    {
        $data["categories"] = Categories::all();
        $data["promotionid"] = $categorywisediscountid;
        $data["category_wise_discounts"] = subcategory_promotions::where('id', $categorywisediscountid)->first();
        $data["promotionselectedproducts"] = products::where('all_offers', $data["category_wise_discounts"]->id)->where("promotion_type","subcategory")->distinct()->get(['sub_category_id']);

//        foreach($aa as $aaa)
//        {
//
//        }
//        $subcategoriesids = array();
//
//        distinct()->get(['column_name']);
//
//        for($a = 0; $a < $aa; $a++)
//        {
//            if()
//        }
//
//        array_push($a,"blue","yellow");
//        foreach()

        //$data["promotionselectedproducts"];

        return view('promotion.sub_categories_wise_discountedit', $data );
    }

    function sub_category_offerseditpost(Request $request)
    {
        $categorywisediscountid = $request->categorywisediscountid;

        $updateColumns = array(
            'offer_name'=>$request->offer_name,
            'enable_on_utc'=>$request->start_date,
            'disable_on_utc'=>$request->end_date,
            'reduction_type'=>$request->reduction_type,
            'percentage'=>$request->reduction_amount,
            'status'=>$request->enabled_1,
            'category_id'=>$request->primary_category,
            'sub_category_id'=>$request->secondary_category
        );

        $category_promotions = new subcategory_promotions;
        $category_promotions::where('id', $categorywisediscountid)->update($updateColumns);

        //copy here
        $products = new products;

        $totalnumberofsubcategory = $request->numberofsubcategory;

        for($a = 1; $a <= $totalnumberofsubcategory; $a++)
        {
            $b = "subcategory".$a;
            $subcategoryid =  $request->$b;

            $updateColumns = array(
                'all_offers'=>$categorywisediscountid,
                'promotion_type'=>'subcategory'
            );

            $products::where('sub_category_id', $subcategoryid)->update($updateColumns);
        }


//        products::where('categories_id', $request->primary_category)->where('sub_category_id',$request->secondary_category)->update(['all_offers' => $request->categorywisediscountid , 'promotion_type' => 'subcategory']);

        return redirect('admin/sub_category_offers')->with('success','Successfully Update Categories Offers');
    }

    function sub_category_wise_offersformpost(Request $request)
    {
        $promotion = products::where('promotion_type','product')->orWhere('promotion_type', 'subcategory')->first();
        if(gettype($promotion) == "object")
        {
            return back()->with('danger','Only one promotion can be added please remove promotion to add new promotion');
        }
        else
        {
            $sub_category_promotions = new subcategory_promotions;
            $sub_category_promotions->offer_name = $request->offer_name;
            $sub_category_promotions->enable_on_utc = $request->start_date;
            $sub_category_promotions->disable_on_utc = $request->end_date;
            $sub_category_promotions->reduction_type = $request->reduction_type;
            $sub_category_promotions->percentage = $request->reduction_amount;
            $sub_category_promotions->status = $request->enabled_1;
            $sub_category_promotions->category_id = $request->primary_category;
            $sub_category_promotions->sub_category_id = $request->secondary_category;
            $sub_category_promotions->save();

            $products = new products;

            $subcategorypromotionid = $sub_category_promotions::max('id');

            $totalnumberofproducts = $request->numberofsubcategory;



            for($a = 1; $a <= $totalnumberofproducts; $a++)
            {
                $b = "subcategory".$a;
                $subcategoryid =  $request->$b;

                echo $subcategoryid."<br>";

                $updateColumns = array(
                    'all_offers'=>$subcategorypromotionid,
                    'promotion_type'=>'subcategory'
                );

                $products::where('sub_category_id', $subcategoryid)->update($updateColumns);
            }

            return redirect('admin/sub_category_offers')->with('success','Successfully Crearte Categories Offers');
        }
    }

    function sub_categorire_offers_delete($id)
    {

        $category_promotions = subcategory_promotions::find($id);

        $category_promotions->delete();

        $updateColumns = array(
            'all_offers'=>'',
            'promotion_type'=>''
        );

        $products = new products;
        $products::where('all_offers', $id)->where('promotion_type','subcategory')->update($updateColumns);


        return back()->with('success','Successfully Deleted Categories Offers');

    }
    function brandoffersform()
    {
        $brans_promo = Brands::all();
        return view('promotion.brandofferform',compact('brans_promo'));
    }

    function brandoffers()
    {
        $data["brand_wise_discounts"] = brand_promotions::all();
        return view('promotion.brand_wise_discount', $data);
    }

    function brandoffersformpost(Request $request)
    {
        $brand_promotions = new brand_promotions;

        $brand_promotions->offer_name = $request->offer_name;
        $brand_promotions->enable_on_utc = $request->start_date;
        $brand_promotions->disable_on_utc = $request->end_date;
        $brand_promotions->reduction_type = $request->reduction_type;
        $brand_promotions->percentage = $request->reduction_amount;
        $brand_promotions->status = $request->enabled_1;
        $brand_promotions->brand_id = $request->brand_id;
        $brand_promotions->save();


        $brands_promo_offer = products::where('brands',$request->brand_id)->update(['all_offers' => $brand_promotions->id, 'promotion_type' => 'brands']);

        return redirect('admin/brand_offers')->with('success','Successfully Crearte Brands Offers');
    }


    function brandoffersedit($brandofferdiscoutid)
    {
        $data["brandwisediscount"] = brand_promotions::where('id', $brandofferdiscoutid)->first();
        $brans_promo = Brands::all();
        return view("promotion.editbrandwisediscount", $data,compact('brans_promo'));
    }

    function brandofferseditpost(Request $request)
    {
        $branddiscountid = $request->brandwisediscountid;
        $offer = brand_promotions::findOrFail($request->brandwisediscountid);

        $brands_promo_offer = products::where('brands',$offer->brand_id)->update(['all_offers' => null]);

        $updateColumns = array(
            'offer_name'=>$request->offer_name,
            'enable_on_utc'=>$request->start_date,
            'disable_on_utc'=>$request->end_date,
            'reduction_type'=>$request->reduction_type,
            'percentage'=>$request->reduction_amount,
            'status'=>$request->enabled_1,
            'brand_id'=>$request->brand_id
        );
       
        $offer->update($updateColumns);

        $brands_promo_offer = products::where('brands',$request->brand_id)->update(['all_offers' => $offer->id, 'promotion_type' => 'brands']);

        return redirect('admin/brand_offers')->with('success','Successfully Update Brands Offers');
    }

    function brandoffers_delete($id)
    {
        $brands_offers = brand_promotions::find($id);

        $brands_offers->delete();

        $updateColumns = array(
            'all_offers'=>'',
            'promotion_type'=>''
        );

        $products = new products;
        $products::where('all_offers', $id)->where('promotion_type','brands')->update($updateColumns);


        return back()->with('success','Successfully Deleted Brands Offers');

    }

    function getsubcategoryproducts(Request $request)
    {

        $products = new products;

        $data["products"] = $products::where('sub_category_id', $request->subcategoryid)->get();

        foreach($data["products"] as $product)
        {
            echo "<option value='$product->id'>".$product->prod_name."</option>";

        }

    }

    function getsubcategoryproducts1(Request $request)
    {
        $products = new products;

        $data["products"] = $products::where('sub_category_id', $request->subcategoryid)->get();

        foreach($data["products"] as $product)
        {
            echo "<option value='$product->id'>".$product->prod_name."</option>";

        }

    }

    function productpromotionremoveproduct(Request $request)
    {
        ;
        $products = new products;

        $updateColumns = array(
            'all_offers'=>'',
            'promotion_type'=>''
        );

        $products::where('id', $request->productid)->update($updateColumns);
    }


    function subcategorypromotionremovecategory(Request $request)
    {

        $products = new products;

        $updateColumns = array(
            'all_offers'=>'',
            'promotion_type'=>''
        );

        $products::where('sub_category_id', $request->subcategoryid)->update($updateColumns);
    }





}
