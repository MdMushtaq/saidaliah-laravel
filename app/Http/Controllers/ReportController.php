<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\products;
use App\Productattributes;
use App\Popularproducts;
use App\Orderitem;
use App\Orders;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $best_sell = products::with([
            'productQuantity' => function ($q)
            {
                $q->withCount([
                    'order_items as items_quantity' => function ($q2)
                    {
                        $q2->select(DB::raw("SUM(product_quantity) as itemsquantity"));
                    }
                ]);
            }
        ])->get();

       
        return view("report.products_report",compact('best_sell'));
    }
    public function SalesReport()
    {
        $orders = Orders::with('orderitems')->where('status','Delivered')->get();

        return view("report.sales_report",compact('orders'));
    }

    public function SalesReportPost(Request $request)
    {
        $betweenDates = "DATE(created_at) BETWEEN '".$request->best_sellers_start."' and '".$request->best_sellers_end."'";
        $orders = Orders::with('orderitems')->where('status','Delivered')->whereRaw($betweenDates)->get();

        return view("report.sales_report",compact('orders'));
    }

    public function productsalesreport()
    {
        $orders = Orders::with('orderitems')->where('status','Delivered')->get();
        return view("report.product_sales_report",compact('orders'));
    }

    public function productsalesreportPost(Request $request)
    {
        $betweenDates = "DATE(created_at) BETWEEN '".$request->best_sellers_start."' and '".$request->best_sellers_end."'";
        $orders = Orders::with('orderitems')->where('status','Delivered')->whereRaw($betweenDates)->get();

        return view("report.product_sales_report",compact('orders'));
    }

    public function vatreport()
    {
        $order = Orders::select(DB::raw('sum(vat) as totalvat'))->where('status','Delivered')->get();
        $totalVat =  $order[0]->totalvat;

        $headingTitle = "Order";
        $orders = Orders::with('orderitems')->where('status','Delivered')->orderBy('id', 'DESC')->get();
        return view("report.vatreport", compact('orders','headingTitle','totalVat'));
    }

    public function vatreportpost(Request $request)
    {
        $betweenDates = "status = 'Delivered' && DATE(created_at) BETWEEN '".$request->best_sellers_start."' and '".$request->best_sellers_end."'";
        $order = Orders::select(DB::raw('sum(vat) as totalvat'))->whereRaw($betweenDates)->get();
        $totalVat =  $order[0]->totalvat;
        $orders = Orders::with('orderitems')->whereRaw($betweenDates)->orderBy('id', 'DESC')->get();
        return view("report.vatreport", compact('orders','totalVat'));
    }

    public function exporttoexcelvatreport(Request $request)
    {
//        $orderids = $request->order_id;
//
//        print_r($orderids);
//        exit;

        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=vatreport.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo '<table border="1">';
        //make the column headers what you want in whatever order you want
        echo '<tr><th>Order Date</th><th>Customer Name</th><th>Total</th><th>Vat</th><th>Grand Total</th></tr>';
        //loop the query data to the table in same order as the headers

        $totalVat = 0;

//        foreach($orderids as $orderid)
//        {
            $orders = Orders::with('orderitems')->where('status','Delivered')->orderBy('id', 'DESC')->get();

        foreach($orders as $order)
        {
            echo "<tr><td>".$order->created_at->format('Y-m-d')."</td><td>".$order->user->name."</td><td>".$order->vat."</td><td>".$order->total."</td><td>".$order->subtotal."</td></tr>";

            $orderr = Orders::select(DB::raw('sum(vat) as totalvat'))->where('status','Delivered')->get();
            $totalVat =  $totalVat+$orderr[0]->totalvat;
        }

//        }

        echo "<tr>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td>Total Vat</td>";
        echo "<td>".$totalVat."</td>";
        echo "</tr>";

        echo '</table>';
    }

    public function exporttoexcelproductsreport(Request $request)
    {
        $productids = $request->productids;

        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=productsreport.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo '<table border="1">';
        //make the column headers what you want in whatever order you want
        echo '<tr><th>Product Name</th><th>Product Color(s)</th><th>Color Quantity</th><th>Sale</th><th>Balance</th></tr>';
        //loop the query data to the table in same order as the headers

            $best_sell = products::with([
                'productQuantity' => function ($q)
                {
                    $q->withCount([
                        'order_items as items_quantity' => function ($q2)
                        {
                            $q2->select(DB::raw("SUM(product_quantity) as itemsquantity"));
                        }
                    ]);
                }
            ])->get();


            foreach ($best_sell as $report)
            {

            $names = [];
            $quantities = [];
            $saleQuantities = [];
            $balanceQuantities = [];

                foreach ($report->productQuantity as $attribute)
                {
                    $attribute->items_quantity = $attribute->items_quantity ?? 0;
                    $names[] = $attribute->colourname;
                    $quantities[] = $attribute->quantity ?? 0;
                    $saleQuantities[] = $attribute->items_quantity;
                    $balanceQuantities[] = $attribute->quantity - $attribute->items_quantity;
                }

                echo "<tr>";
                echo "<td>".$report->prod_name."</td>";
                echo "<td>".implode(', ', $names)."</td>";
                echo "<td>".implode(', ', $quantities)."</td>";
                echo "<td>".implode(', ', $saleQuantities)."</td>";
                echo "<td>".implode(', ', $balanceQuantities)."</td>";
                echo "</tr>";
            }

        echo "</table>";

    }

    public function exporttoexcelsalesreport(Request $request)
    {
        // $orderids = $request->order_id;
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=salesreport.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo '<table border="1">';
        //make the column headers what you want in whatever order you want
        echo '<tr><th>Date</th><th>Full Name</th><th>Item</th><th>Unit Price</th><th>Quantity</th><th>Amount</th></tr>';
        //loop the query data to the table in same order as the headers
        $total = 0;

//        foreach($orderids as $orderid)
//        {
            $orders = Orders::with('orderitems')->where('status','Delivered')->get();

            foreach($orders as $order)
            {
                foreach ($order->orderitems as $item)
                {
                    echo "<tr>";
                    echo "<td>".$order->created_at->format('d/m/Y')."</td>";
                    echo "<td>".$order->user->name."</td>";
                    echo "<td>".$item->product_name."</td>";
                    echo "<td>".$item->product_unit_price."</td>";
                    echo "<td>".$item->product_quantity."</td>";
                    echo "<td>".$item->product_unit_price*$item->product_quantity."</td>";
                    echo "</tr>";

                    $totalAmount = $item->product_unit_price*$item->product_quantity;
                    $total = $total+$totalAmount;
                }
            }
//        }

        echo "<tr>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td>Total Amount</td>";
        echo "<td>".$total."</td>";
        echo "</tr>";
        echo "</table>";
    }

    public function exporttoexcelproductsalesreport(Request $request)
    {
//        $orderids = $request->order_id;

        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=productsalesreport.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo '<table border="1">';
        //make the column headers what you want in whatever order you want
        echo '<tr><th>Date</th><th>Full Name</th><th>Item</th><th>Unit Price</th><th>Quantity</th><th>Amount</th></tr>';
        //loop the query data to the table in same order as the headers

        $total = 0;

//        foreach($orderids as $orderid)
//        {
            $orders = Orders::with('orderitems')->where('status','Delivered')->get();

        foreach($orders as $order)
        {
            foreach($order->orderitems as $item)
            {
                echo "<tr>";
                echo "<td>".$order->created_at->format('d/m/Y')."</td>";
                echo "<td>".$order->user->name."</td>";
                echo "<td>".$item->product_name."</td>";
                echo "<td>".$item->product_unit_price."</td>";
                echo "<td>".$item->product_quantity."</td>";
                echo "<td>".$item->product_unit_price*$item->product_quantity."</td>";
                echo "</tr>";

                $totalAmount = $item->product_unit_price*$item->product_quantity;
                $total = $total+$totalAmount;
            }
        }
//        }

        echo "<tr>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td>Total Amount</td>";
        echo "<td>".$total."</td>";
        echo "</tr>";
        echo "</table>";


    }
}
