<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;
use App\User;
use App\Orderitem;
use Illuminate\Support\Facades\Mail;
use App\Mail\AramexMail;

class SalesController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $headingTitle = "Order";
        $orders = Orders::with('orderitems')->where('approved_status','')->orderBy('id', 'DESC')->get();
        return view("sales.orders", compact('orders','headingTitle'));
    }

    public function confirmbyAdmin()
    {
        $headingTitle = "Confirm By Admin";
        $orders = Orders::with('orderitems')->where('approved_status','Confirm_byAdmin')->orderBy('id', 'DESC')->get();
        return view("sales.orders", compact('orders','headingTitle'));
    }

    public function Orderdelivered()
    {
        $headingTitle = "Delivered";
        $orders = Orders::with('orderitems')->where('status','Delivered')->orderBy('id', 'DESC')->get();
        return view("sales.orders", compact('orders','headingTitle'));
    }

    // public function confirmbyajent()
    // {
    //     $orders = Orders::where('approved_status','Confirm_byAjent');
    //     return view("sales.orders", compact('orders'));
    // }

    public function show($id)
    {
       
       $itemorder = Orders::with('orderitems')->where('id',$id)->orderBy('id', 'DESC')->get();
       return view("sales.orderitems", compact('itemorder'));
    }

    public function update(Request $request ,$id)
    {
        // dd($request);
        

        $order = Orders::find($id);
        $order->status =  $request->order_status;
        $order->approved_status = $request->order_approved_status;
        $order->aramex_ref = $request->aramex_ref;
        $order->aramex_track = $request->aramex_tracking;
        $order->shipping_date = $request->shipping_date;
        $order->save();

        $data = [
            'aramex_track' => $order->aramex_track,
            'email'=>$request->user_email,
        ];

        if($request->aramex_tracking != '')
        {
            Mail::to($request->user_email)->send(new AramexMail($data));
        }

        return redirect('/admin/orders');
    }

    public function exporttoexcel(Request $request)
    {
        $orderids = $request->orderids;

        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=ordersreport.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo '<table border="1">';
        //make the column headers what you want in whatever order you want
        echo '<tr><th>Order Date</th><th>Customer Name</th><th>Order Status</th><th>Payment</th><th>Status</th><th>Total</th><th>VAt</th><th>Grant total</th><th>Action</th></tr>';
        //loop the query data to the table in same order as the headers

        foreach($orderids as $orderid)
        {
            $order = Orders::with('orderitems')->where('approved_status','')->where('id',$orderid)->first();

            echo "<tr>";
            echo "<td>".$order->created_at->format('Y-m-d')."</td>";
            echo "<td>".$order->user->name."</td>";
            echo "<td>".$order->status."</td>";
            echo "<td>".$order->payment."</td>";
            echo "<td>";
            if($order->approved_status == '')
            {
                echo "Not Approved";
            }
            else if($order->approved_status == "Confirm_byAdmin")
            {
                echo "Confirm By Admin";
            }
            else if($order->approved_status == "Confirm_byAjent")
            {
                echo "Confirm By Ajents";
            }
            echo "</td>";
            echo "<td>".$order->vat."</td>";
            echo "<td>".$order->total."</td>";
            echo "<td>".$order->subtotal."</td>";
            echo "</tr>";
        }
        echo "</table>";
    }

}
