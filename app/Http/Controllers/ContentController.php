<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\banners;


class ContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    function index()
    {
        $banners = new banners;
        $data["banners"] = $banners::all();
        return view('content.banner', $data);
    }

    function create()
    {
        return view('content.banneradd');
    }

    function createpost(Request $request)
    {
        $banners = banners::get()->count();
        if($banners>=5)
        {
            return back()->with('bannerslimit','More than 5 banners can not be uploaded');
        }
        else
        {
            $request->validate([
                'banner_image' => 'required',
                'banner_arabic_image' => 'required'
            ]);

            $imagename = request()->banner_image->getClientOriginalName();

            request()->banner_image->move(public_path('images/banners/'), $imagename);

            $arabicimagename = request()->banner_arabic_image->getClientOriginalName();

            request()->banner_arabic_image->move(public_path('images/banners/'), $arabicimagename);

            $banner = new banners;
            $banner->banner_img = $imagename;
            $banner->banner_arabic_img = $arabicimagename;
            $banner->banner_link = $request->slider_link;
            $banner->save();

            return redirect('admin/banners');
        }
    }

    function banneredit($bannerid)
    {
        $banner = new banners;
        $data["banner"] = $banner::where('id',$bannerid)->first();
        return view('content.banneredit', $data);
    }

    function bannereditpost(Request $request)
    {
        $banners = banners::find($request->bannerid);

        if($request->hasfile('banner_image'))
        {
            $request->validate([
                'banner_image' => 'required|image|mimes:jpeg,png,jpg'
            ]);

            $imagename = request()->banner_image->getClientOriginalName();
            request()->banner_image->move(public_path('images/banners/'), $imagename);
            $banners->banner_img = $imagename;
        }
        if($request->hasfile('banner_arabic_image'))
        {
            $request->validate([
                'banner_arabic_image' => 'required|image|mimes:jpeg,png,jpg'
            ]);
            $arabicimagename = request()->banner_arabic_image->getClientOriginalName();
            request()->banner_arabic_image->move(public_path('images/banners/'), $arabicimagename);
            $banners->banner_arabic_img = $arabicimagename;
        }
        $banners->banner_link = $request->slider_link;
        $banners->save();

        return redirect('admin/banners');
    }

    public function deletebanner($id)
    {
        $banner = banners::find($id);
        $banner->delete();
        return redirect('admin/banners')->with('danger','Banner Deleted');
    }

}
