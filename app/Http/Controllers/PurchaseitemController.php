<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Purchaseitem;

class PurchaseitemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $itempurchase = Purchaseitem::all();
        return view('purchaseproduct.purchaseitem',compact('itempurchase'));
    }


    public function create()
    {
        return view('purchaseproduct.addpurchaseitem');
    }

   
    public function store(Request $request)
    {
        $itempurchase = new Purchaseitem;
        $itempurchase->date = $request->date;
        $itempurchase->from = $request->from;
        $itempurchase->item = $request->item;
        $itempurchase->purchase_amount	 = $request->p_amount;
        $itempurchase->model_num = $request->model_no;
        $itempurchase->qty = $request->qty;
        $itempurchase->price = $request->price;
        $itempurchase->save();

        return redirect('/admin/purchase/list')->with('success','Successfully added Purchase item');


    }

  
    public function show($id)
    {
    }

    public function edit($id)
    {
        $itempurchase = Purchaseitem::where('id',$id)->first();
        return view('purchaseproduct.editpurchaseitem', compact('itempurchase'));
    }

    public function editPost(Request $request)
    {
        $itempurchase = Purchaseitem::where('id',$request->itempuchaseid)->first();
        $itempurchase->date = $request->date;
        $itempurchase->from = $request->from;
        $itempurchase->item = $request->item;
        $itempurchase->purchase_amount	 = $request->p_amount;
        $itempurchase->model_num = $request->model_no;
        $itempurchase->qty = $request->qty;
        $itempurchase->price = $request->price;
        $itempurchase->save();

        return redirect('/admin/purchase/list')->with('success','Successfully edited Purchase item');

    }

    public function delete($id)
    {
        $itempurchase = Purchaseitem::where('id',$id)->first();
        $itempurchase->delete();


        return redirect('/admin/purchase/list')->with('success','Successfully deleted Purchase item');
    }


  
    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
       
    }
}
