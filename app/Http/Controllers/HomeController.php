<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Categories;
use App\subcategories;
use Cart;
use App\products;
use App\Wishlist;
use App\User;
use App\Orders;
use App\Orderitem;
use App\City;
use App\Transaction;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use App\Warranty;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except([
            // 'cartadd'
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orderitem = Orders::with('orderitems')->where('user_id',auth()->id())->orderBy('id', 'DESC')->get();

        $wishlist = Wishlist::with('products')->where('user_id',auth()->id())->orderBy('id', 'DESC')->get();
        return view('front-end.customer_dashboard',compact('orderitem','wishlist'));
    }


    public function wishlist($id)
    {
       $wishlist = new Wishlist;
       $wishlist->product_id = $id;
       $wishlist->user_id = auth()->id();
       $wishlist->save();

       return back()->with('success','Successfully Added to Your Wishlist');

    }
    public function checkoutForm(Request $request)
    {
        $cart =  Cart::content();
        $products = products::whereIn('id', $cart->pluck('id')->toArray())->get();
        foreach ($cart as $item) {
            $options = $item->options->toArray();
            unset($options['original_price']);
            $product = $products->where('id', $item->id)->first();
            $item->product = $product;
            $item->total = $item->qty * $item->price;
            $item->attributes = $options;
        }
        $customerinfo = auth()->user();       
        $city = City::with('Shipping')->where('id', $request->session()->get('city_id', $customerinfo->city))->first(); 
        return view('front-end.checkout-form',compact('cart', 'customerinfo','city'));
        # code...
    }

    public function CityChange(Request $request)
    {
      $cityId = $request->input('city_id');
      $city = City::with('Shipping')->whereHas('Shipping')->find($cityId);
      if ($city) {
            $subTotal = Cart::subtotal();
            $vat = number_format((config('cart.tax') * $subTotal) / 100, 2);
            $shipping = number_format($city->Shipping->shippingrate, 2);
            $total = number_format($subTotal + $vat + $shipping,2);
            $request->session()->put('city_id', $city->id);
        return response()->json([
          'shipping' => $city->name != 'Other' ? trans('checkout-form.SR').' '.$shipping : 'Shipping with Aramax',
          'total' => trans('checkout-form.SR').' '.$total,
        ]);
      }
      return response()->json([
        'message' => 'We currently do not ship to this city!'
      ], 422);
    }

    public function Checkoutpost(Request $request)
    {
        $cartItems = Cart::content();
        if ($cartItems->count() === 0) {
          return redirect('/')->with('error', 'Your cart is empty!');
        }
        $city = City::with('Shipping')->where('id', $request->session()->get('city_id', auth()->user()->city))->first(); 
        $subTotal = Cart::subtotal();
        $vat = number_format((config('cart.tax') * $subTotal) / 100, 2);
        $shipping = number_format($city->Shipping->shippingrate, 2);
        $total = number_format($subTotal + $vat + $shipping, 2);

        $order = new Orders;
        $order->user_id = auth()->id();
        $order->shipping_name = $request->shipping_name;
        $order->shipping_phone = $request->shipping_phone;
        $order->shipping_address = $request->shipping_address;
        $order->status = 'Pending';
        $order->approved_status = '';
        $order->payment = 'Not done';
        $order->vat = $vat;
        $order->total = $total;
        $order->shipping_price = $shipping;
        $order->subtotal = $subTotal;
        $order->aramex_ref = '';
        $order->aramex_track = '';
        $order->shipping_date = '';
        $order->save();

        foreach($cartItems as $item)
        {
            $options = $item->options->toArray();
            $original_price = $options['original_price'];
            unset($options['original_price']);
            $color = isset($options['color']) ? $options['color'] : (object)[];
            $items = new Orderitem;
            $items->order_id = $order->id;
            $items->product_id = $item->id;
            $items->product_name = $item->name;
            $items->product_unit_price = $item->price;
            $items->product_quantity = $item->qty;
            $items->product_price = $item->price * $item->qty;
            $items->color_id = optional($color)->id;
            $items->product_color = optional($color)->name;
            $items->save();

            $endwarranty = 0;
            $warrantyclaim = products::find($item->id);
            if($warrantyclaim->warrantytype == 'year'){
              $endwarranty = Carbon::now()->addYear($warrantyclaim->warrantyduration);
            
            }
            elseif($warrantyclaim->warrantytype == 'month'){
              $endwarranty = Carbon::now()->addMonth($warrantyclaim->warrantyduration);
            }
            $warranty = new Warranty();
            $warranty->orderitem_id = $items->id;
            $warranty->warrantyend = $endwarranty;
            $warranty->save();
        }
        Cart::destroy();

        return redirect()->route('checkout.pay', encrypt($order->id));
    }

    public function payView(Request $request, $orderId)
    {
      try {
        $order = Orders::with([
          'user',
          'orderitems' => function ($q)
          {
            $q->with('product');
          }
        ])->findOrFail(decrypt($orderId));
      } catch (Exception $e) {
        abort(404);
      }
      return view('front-end.transaction', compact('order'));
    }

    // public function pay()
    // {
    //     return view('front-end.api-test');
    // }
    
    public function CustomerDashboard()
    {
       $orderitem = Orders::with('orderitems')->orderBy('id', 'DESC')->get();

       $wishlist = Wishlist::with('products')->where('user_id',auth()->id())->orderBy('id', 'DESC')->get();
        return view('front-end.customer_dashboard',compact('orderitem','wishlist'));
    }
    public function show($id)
    {
       $itemorder = Orders::with('orderitems')->where('id',$id)->orderBy('id', 'DESC')->get();
       return view("front-end.cartitems", compact('itemorder'));
    }

    public function payredirect(Request $request)
    {
      // dd(Cache::get('tap.test'));
      return view('front-end.payredirect');
    }

    public function tapPost(Request $request)
    {
      dd($request->all());
      Cache::forever('tap.test', $request->all());
    }

    public function tapCharge(Request $request)
    {
      $id = $request->input('id');
      $client = new Client();
      $response = $client->request('GET','https://api.tap.company/v2/charges/'.$id, [
        'headers' => [
          'Authorization' => 'Bearer sk_test_CceoE46KA3gx9MTNjhRk2uiY',
          'Content-Type'=>'application/json'
        ]
      ]);
      $response = json_decode($response->getBody()->getContents());
      $order = Orders::findOrFail($response->reference->order);
      if ($order->payment == 'Not done' && $response->status == 'CAPTURED') {
        $order->payment = 'Success';
        $order->save();
        $trans = new Transaction;
        $trans->order_id = $order->id;
        $trans->total = $response->amount;
        $trans->status = $response->status;
        $trans->data = json_encode($response);
        $trans->save();
      }    
        
    }
}


