<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Permission;
use Auth;
use App\Vat;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\City;
use App\Freeshippings;
use App\Flaterates;
use App\Faq;
use App\Faqcategories;
use App\Subscriptions;
use Illuminate\Support\Facades\Cache;


class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index()
    {
        $admins = Admin::all();
        return view('settings.administrator',compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $this->authorize('Admin Create');

        $permissions = Permission::with('children')->whereNull('parent_id')->get();
        return view('settings.create_admins', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $this->validate($request, [
            'name'   => 'required',
            'email'   => 'required|email|unique:admins',
            'password' => 'required|min:6|confirmed'
        ]);

        $admins = new Admin;
        $admins->name = $request->name;
        $admins->email = $request->email;
        $admins->job_title = 'sub-admin';
        $admins->password = Hash::make($request->password);
        $admins->save();

        $admins->permissions()->sync(array_keys($request->permissions));

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function show(Settings $settings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function edit(Settings $settings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Settings $settings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Settings $settings)
    {
        //
    }

    public function vatformadd()
    {
        $data["vats"] = Vat::all();
        $data["maxVatId"] = Vat::max('id');
        return view("settings.vatform", $data);
    }

    public function vatformaddpost(Request $request)
    {
        Cache::forget('vat');
        $rate = $request->rate;
        $selectrate = $request->selectrate;
        $vatid = $request->vatid;

        $vat = new Vat;

        $index = 0;

        if(gettype($rate) == "array")
        {
            foreach($rate as $ratt)
            {
                $checkingValue = $vat::where('vatpercentage', $ratt)->first();
                if(gettype($checkingValue) == "object")
                {

                }
                else
                {
                    $vat = new Vat;
                    $vat->vatpercentage = $ratt;
                    $vat->vatselect = 0;
                    $vat->save();
                }
            }

            if($selectrate)
            {

                DB::select('update vat set vatselect = 0');

                $updateColumn = array(
                    'vatselect' => 1
                );
                $vat::where('id', $selectrate)->update($updateColumn);


            }
            else
            {

            }

            return redirect('admin/vat/form');
        }
        else
        {
            return back()->with('danger','Vat not added something went wrong');
        }
    }


    function getmaxvatid(Request $request)
    {
        $maxVatId = Vat::max('id');
        echo $maxVatId;
    }

    public function deletevat($id)
    {
        $brands = Vat::find($id);
        $brands->delete();

        return back()->with('danger','Successfully Deleted Brands');

    }

    public function freeshippinformadd()
    {
        $city = City::with('Shipping')->orderBy('id', 'DESC')->get();
        return view("settings.shippingform",compact('city'));
    }
    public function Shippingcreate()
    {
        return view('settings.create_shipping');
    }
    public function ShippingStore(Request $request)
    {
      $city = new City();
      $city->name = $request->name;
      $city->arabic_name = $request->arabic_name;
      $city->status = $request->status;
      $city->save();

      $ship_rate = new Freeshippings;
      $ship_rate->city_id = $city->id;
      $ship_rate->shippingrate = $request->shipping_rate;
      $ship_rate->save();

      return redirect('/admin/shipping/list')->with('Successfully created Shipping Rate ');
    }
    
    public function freeshippinedit($id)
    {
        $city = City::find($id);
        $shipping = Freeshippings::where('city_id',$city->id)->first();
        return view('settings.update_shipping',compact('city','shipping'));
    }

    public function ShippingUpdate(Request $request,$id)
    {
        // dd($request->all());
      $city = City::find($id);
      $city->name = $request->name;
      $city->arabic_name = $request->arabic_name;
      $city->status = $request->status;
      $city->save();
      $shipping = Freeshippings::where('city_id',$city->id)->update(['shippingrate'=>$request->shipping_rate]);
      return redirect('/admin/shipping/list')->with('Successfully created Shipping Rate ');
    }

    // public function getmaxshippingid()
    // {
    //     $maxVatId = Freeshippings::max('id');
    //     echo $maxVatId;
    // }

    // public function freeshippinformaddpost(Request $request)
    // {
    //     $rate = $request->rate;
    //     $selectrate = $request->selectrate;
    //     $shippingid = $request->shippingid;

    //     $freeshipping = new Freeshippings;

    //     $index = 0;

    //     if(gettype($rate) == "array")
    //     {
    //         foreach($rate as $ratt)
    //         {

    //             $checkingValue = $freeshipping::where('shippingrate', $ratt)->first();
    //             if(gettype($checkingValue) == "object")
    //             {

    //             }
    //             else
    //             {
    //                 $freeshipping = new Freeshippings;
    //                 $freeshipping->shippingrate = $ratt;
    //                 $freeshipping->shipselect = 0;
    //                 $freeshipping->save();
    //             }
    //         }
    //         if($selectrate)
    //         {

    //             DB::select('update freeshipping set shipselect = 0');

    //             $updateColumn = array(
    //                 'shipselect' => 1
    //             );
    //             $freeshipping::where('id', $selectrate)->update($updateColumn);

    //         }
    //         else
    //         {

    //         }

    //         return redirect('admin/free-shipping/form');
    //     }
    //     else
    //     {
    //         return back()->with('danger','Shipping not added something went wrong');
    //     }

    // }

    // public function deletefreeshipping($id)
    // {
    //     $brands = Freeshippings::find($id);
    //     $brands->delete();

    //     return back()->with('danger','Successfully Deleted Brands');

    // }

    public function flatrateformadd()
    {
        $data["flatrates"] = Flaterates::all();
        $data["maxFlatrateId"] = Flaterates::max('id');
        return view("settings.flatrateform", $data);
    }

    public function flatrateformaddpost(Request $request)
    {
        $rate = $request->rate;
        $selectrate = $request->selectrate;
        $flatrateid = $request->flatrateid;

        $flatrates = new Flaterates;

        $index = 0;

        if(gettype($rate) == "array")
        {
            foreach($rate as $ratt)
            {

                $checkingValue = $flatrates::where('flatrate', $ratt)->first();
                if(gettype($checkingValue) == "object")
                {

                }
                else
                {
                    $flatrates = new Flaterates;
                    $flatrates->flatrate = $ratt;
                    $flatrates->flatrateselect = 0;
                    $flatrates->save();
                }
            }

            if($selectrate)
            {

                DB::select('update flaterates set flatrateselect = 0');

                $updateColumn = array(
                    'flatrateselect' => 1
                );
                $flatrates::where('id', $selectrate)->update($updateColumn);

            }
            else
            {

            }

            return redirect('admin/flat-rate/form');
        }
        else
        {
            return back()->with('danger','Flat rate not added something went wrong');
        }
    }

    public function deleteflatrate($id)
    {
        $flatrate = Flaterates::find($id);
        $flatrate->delete();
        return back()->with('danger','Successfully Deleted Brands');
    }

    public function faq()
    {
        $faqs = Faq::all();
        $faqcategories = Faqcategories::all();
        return view('settings.faq', compact('faqs','faqcategories'));
    }

    public function faqadd()
    {
        $faqs = Faq::all();
        $faqcategories = Faqcategories::all();
        return view('settings.faqaddform', compact('faqs','faqcategories'));
    }

    public function faqaddpost(Request $request)
    {
        $faq = new Faq;
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->arabicquestion = $request->arabicquestion;
        $faq->arabicanswer = $request->arabicanswer;
        $faq->faqcategoryid = $request->faqcategoryid;
        $faq->save();

        return back()->with('success','Record Added');
    }

    public function faqedit($faqid)
    {
        $faq = Faq::find($faqid);
        $faqcategories = Faqcategories::all();
        return view('settings.faqeditform', compact('faq','faqcategories'));
    }

    public function faqeditpost(Request $request)
    {
        $faq = Faq::find($request->faqid);
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->arabicquestion = $request->arabicquestion;
        $faq->arabicanswer = $request->arabicanswer;
        $faq->faqcategoryid = $request->faqcategoryid;
        $faq->save();

        return redirect ('admin/faq');
    }

    public function faqdelete($faqid)
    {
        $faq = Faq::find($faqid);
        $faq->delete();

        return redirect('/admin/faq');
    }

    public function faqcategoies()
    {
        $faqcategories = Faqcategories::all();
        return view('settings.faqcategories',compact('faqcategories'));
    }

    public function faqaddcategory()
    {
        return view('settings.faqaddcategory');
    }

    public function faqaddcategoryPost(Request $request)
    {
        $this->validate($request, [
            'categoryname'   => 'required',
            'arabiccategoryname' => 'required'
        ]);

        $faqcategories = new Faqcategories;
        $faqcategories->category_name = $request->categoryname;
        $faqcategories->arabiccategoryname = $request->arabiccategoryname;
        $faqcategories->save();

        return redirect('/admin/faq');

    }

    public function faqcategoryedit($id)
    {
        $faqcategory = Faqcategories::find($id);
        return view('settings.faqeditcategory',compact('faqcategory'));
    }

    public function faqcategoryeditpost(Request $request)
    {
        $faqcategory = Faqcategories::find($request->faqcategoryid);
        $faqcategory->category_name = $request->categoryname;
        $faqcategory->arabiccategoryname = $request->arabiccategoryname;
        $faqcategory->save();

        return redirect('faq/category');
    }

    public function subscriptions()
    {
        $subscriptions = Subscriptions::all();
        return view('settings.subscriptions',compact('subscriptions'));
    }

    public function generateexcelsheet()
    {
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=filename.xls");
        header("Pragma: no-cache");
        header("Expires: 0");


        echo '<table border="1">';
//make the column headers what you want in whatever order you want
        echo '<tr><th>S.No</th><th>Email</th><th>Created At</th></tr>';
//loop the query data to the table in same order as the headers
        $subscriptions = Subscriptions::all();
        $count = 1;
        foreach($subscriptions as $subscription)
        {
            echo "<tr><td>".$count."</td><td>".$subscription->email."</td><td>".$subscription->created_at."</td></tr>";
            $count++;
        }
        echo '</table>';
    }

    public function generatetonotepad()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=filename.txt");
        header("Pragma: no-cache");
        header("Expires: 0");

        $subscriptions = Subscriptions::all();
        $count = 1;
        foreach($subscriptions as $subscription)
        {
            echo $count." ".$subscription->email." ".$subscription->created_at."\n";
            $count++;
        }
    }



}
