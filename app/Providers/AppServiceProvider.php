<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Cache;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        Schema::defaultStringLength(191);
    }
    public function boot()
    {
        $vat = Cache::remember('vat', 60*60*12, function ()
        {
            return \optional(\App\Vat::where('vatselect', 1)->first())->vatpercentage;
        });
        config(['cart.tax' => $vat ?? 15]);
    }
}
