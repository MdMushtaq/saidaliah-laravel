<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use App\Permission;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->registerPermissions();

        //
    }

    public function registerPermissions()
    {
        $permissions = Permission::all();

        foreach ($permissions as $permission) {
            Gate::define($permission->name, function ($user) use ($permission) {
                return true;
                // return $user->permissions()->where('permissions.id', $permission->id)->first();
            });

            $extraPermissions = [
                'Create',
                'Store',
                'Update',
                'Edit',
                'Delete'
            ];
            foreach ($extraPermissions as $extra) {
                Gate::define($permission->name.'.'.$extra, function ($user) {
                    // return true;
                    return in_array($user->job_title, [
                        'master admin',
                        'admin'
                    ]);
                });
            }
        }
    }
}
