<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    
    protected $table = 'orders';

    
    public function orderitems()
    {
       return $this->hasMany(Orderitem::class, 'order_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getItemsAttribute()
    {
        return $this->orderitems->map(function ($item)
        {
            $color = '';
            if ($item->product_color) {
                $color = ' - '.$item->product_color;
            }
            return [
                'id' => $item->id,
                'name' => $item->product_name.$color,
                'amount_per_unit' =>  $item->product_unit_price,
                'quantity' => $item->product_quantity,
                'total_amount' => $item->product_price,
            ];
        })->toArray();
    }
  
}

